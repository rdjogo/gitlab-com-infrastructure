resource "google_compute_firewall" "windows-manager-deny-executor" {
  name        = "windows-manager-deny-executor"
  direction   = "INGRESS"
  network     = var.network
  priority    = 950
  target_tags = ["windows-manager"]
  source_ranges = [
    "10.2.0.0/16"
  ]
  deny {
    protocol = "all"
  }
}

resource "google_compute_firewall" "runners-manager-to-executors" {
  name        = "runners-manager-to-executors"
  direction   = "INGRESS"
  network     = var.network
  priority    = 1000
  target_tags = ["windows-executor"]

  source_ranges = [
    "10.1.0.0/16"
  ]

  allow {
    protocol = "tcp"
    ports    = ["5985-5986"]
  }
}

resource "google_compute_firewall" "windows-executor-deny-internal" {
  name               = "windows-executor-deny-internal"
  direction          = "EGRESS"
  network            = var.network
  priority           = 500
  target_tags        = ["windows-executor"]
  destination_ranges = ["10.0.0.0/8"]
  deny {
    protocol = "all"
  }
}

resource "google_compute_firewall" "windows-executor-egress" {
  name        = "windows-executor-egress"
  direction   = "EGRESS"
  network     = var.network
  priority    = 1000
  target_tags = ["windows-executor"]

  allow {
    protocol = "tcp"
    ports    = ["80", "443", "22", "1024-65535"]
  }
}


resource "google_compute_firewall" "windows-executor-deny-all-else" {
  name               = "windows-executor-deny-all-else"
  direction          = "EGRESS"
  network            = var.network
  priority           = 5000
  target_tags        = ["windows-executor"]
  destination_ranges = ["0.0.0.0/0"]
  deny {
    protocol = "all"
  }
}

#### Management

resource "google_compute_firewall" "ci-to-managers" {
  name        = "ci-to-managers"
  direction   = "INGRESS"
  network     = var.network
  priority    = 1000
  target_tags = ["windows-manager"]

  source_ranges = [
    "10.3.0.0/16",
    module.bastion.instances[0].network_interface.0.access_config.0.nat_ip
  ]

  allow {
    protocol = "tcp"
    ports    = ["5986", "4421", "3389"]
  }
}

resource "google_compute_firewall" "bastion-to-runner" {
  name        = "bastion-to-runner"
  direction   = "INGRESS"
  network     = var.network
  priority    = 1000
  target_tags = ["runner"]

  source_ranges = [
    "10.3.1.0/24",
  ]

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

### This opens winrm and RDP port for the managers to the world
### This should remain disabled unless troubleshooting is in progress

resource "google_compute_firewall" "testing-manager-winrm" {
  name        = "winrm-to-managers"
  direction   = "INGRESS"
  network     = var.network
  priority    = 1000
  target_tags = ["windows-manager"]
  disabled    = true
  allow {
    protocol = "tcp"
    ports    = ["5986", "3389", "4421"]
  }
}

#### Monitoring

resource "google_compute_firewall" "prometheus" {
  name      = "prometheus"
  direction = "INGRESS"
  network   = var.network
  priority  = 1000

  source_ranges = [
    "13.68.87.12/32", # prometheus.gitlab.com
    "35.185.16.254",  # prometheus-01-inf-gprd
    "34.74.136.38",   # prometheus-02-inf-gprd
    "35.227.109.92",  # gitlab-ops / prometheus-01-inf-ops
    "35.237.131.211", # gitlab-ops /prometheus-02-inf-ops
  ]

  allow {
    protocol = "tcp"
    ports    = ["9182", "9402"]
  }
}

resource "google_compute_firewall" "thanos" {
  name        = "thanos"
  description = "Prometheus Thanos access"
  direction   = "INGRESS"
  network     = var.network
  priority    = 1000

  target_tags = ["prometheus-server"]

  source_ranges = [
    "35.227.109.92/32",  # prometheus-01-inf-ops
    "35.237.131.211/32", # prometheus-02-inf-ops
    "35.237.55.26/32",   # dashboards-01-inf-ops
    "35.237.254.196/32", # dashboards-com-01-inf-ops
    "104.196.117.149",   # runner-chatops-01-inf-ops
  ]

  allow {
    protocol = "tcp"
    ports    = [10901, 10902]
  }
}
