terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/org-ci/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 2.20.0"
}

provider "google-beta" {
  project = var.project
  region  = var.region
  version = "~> 3.26.0"
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

## CloudFlare

provider "cloudflare" {
  version    = "= 2.3"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

provider "random" {
  version = "~> 2.2"
}

module "vfiles" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vfiles.git?ref=v1.4.0"
}

##################################
#
#  Network
#
##################################

module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 2.2"

  project_id   = var.project
  network_name = var.network

  subnets = [
    {
      subnet_name   = "manager"
      subnet_ip     = "10.1.0.0/24"
      subnet_region = var.region
      description   = "Subnet for the runner manager machines"

    },
    {
      subnet_name   = "shared-runner"
      subnet_ip     = "10.2.0.0/16"
      subnet_region = var.region
      description   = "Subnet for all ephemeral runner machines"
    }
  ]
}

resource "google_compute_network_peering" "peering_ops" {
  name         = "peering-ops"
  network      = module.vpc.network_self_link
  peer_network = var.network_ops
}

##################################
#
#  Storage Buckets
#
##################################

module "storage" {
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/ci-storage-buckets.git?ref=v1.2.0"
  environment               = var.environment
  service_account_email     = var.service_account_email
  gcs_service_account_email = var.gcs_service_account_email
  secrets_viewer_access     = ["serviceAccount:${google_service_account.k8s-workloads.email}", "serviceAccount:${google_service_account.k8s-workloads-ro.email}"]
}

##################################
#
#  Service Accounts
#
##################################

# Create the bootstrap KMS key ring
resource "google_kms_key_ring" "bootstrap" {
  name     = "gitlab-${var.environment}-bootstrap"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = google_kms_key_ring.bootstrap.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

# Create the gitlab-secrets KMS key ring
resource "google_kms_key_ring" "gitlab-secrets" {
  name     = "gitlab-secrets"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "secrets" {
  key_ring_id = google_kms_key_ring.gitlab-secrets.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
    "serviceAccount:${google_service_account.k8s-workloads.email}",
    "serviceAccount:${google_service_account.k8s-workloads-ro.email}"
  ]
}

# 7776000s is 90 days

resource "google_kms_crypto_key" "gitlab-secrets" {
  name            = var.environment
  key_ring        = google_kms_key_ring.gitlab-secrets.self_link
  rotation_period = "7776000s"

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_service_account" "runner-manager" {
  account_id   = "runner-manager"
  display_name = "Runner Managers Account"
  description  = "Service account used by the runners manager to spin up executor VMs"
}

resource "google_project_iam_custom_role" "runner-manager" {
  role_id     = "runnerManager"
  title       = "Runner Manager Role"
  description = "Compute service account for CI runner managers"
  permissions = var.runner_manager_role_permissions
}

resource "google_project_iam_member" "runner-manager" {
  role   = google_project_iam_custom_role.runner-manager.id
  member = "serviceAccount:${google_service_account.runner-manager.email}"
}

resource "google_project_iam_member" "runner-manager-iam-user" {
  role   = "roles/iam.serviceAccountUser"
  member = "serviceAccount:${google_service_account.runner-manager.email}"
}

resource "google_service_account" "runner-executor" {
  account_id   = "ephemeral-runner"
  display_name = "Ephermeral runner machine account"
  description  = "Service account assigned to the ephemeral runner machines. Should have no privileges."
}

resource "google_service_account" "stale-ci-cleaner" {
  account_id   = "stale-ci-cleaner"
  display_name = "Stale CI Runners Cleaner Account"
  description  = "Service account used by the script that cleans up stale CI runners"
  # https://ops.gitlab.net/gitlab-com/gl-infra/ci-project-cleaner
}

resource "google_project_iam_custom_role" "stale-ci-cleaner" {
  role_id     = "staleCiCleaner"
  title       = "Stale CI Runners Cleaner role"
  description = "Role for the script that cleans up stale CI runners"
  permissions = var.stale_ci_cleaner_permissions
}

resource "google_project_iam_member" "stale-ci-cleaner" {
  role   = google_project_iam_custom_role.stale-ci-cleaner.id
  member = "serviceAccount:${google_service_account.stale-ci-cleaner.email}"
}

resource "google_service_account" "storage" {
  account_id   = "storage"
  display_name = "GCS CI Cache storage Account"
  description  = "Service account used by the runners to store cache"
}

##################################
#
#  Runner Managers
#
##################################

module "docker-shared-runners-manager" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/ci-manager.git?ref=v1.4.0"

  chef_bootstrap_pem_path    = var.chefpem
  chef_run_list              = "${var.environment}-base-runner"
  dns_zone_name              = var.dns_zone_name
  use_new_node_name          = var.use_new_node_name
  environment                = var.environment
  machine_type               = var.machine_types["managers"]
  name                       = "gitlab-docker-shared-runners-manager"
  node_count                 = var.node_count["managers"]
  os_disk_type               = "pd-standard"
  project                    = var.project
  region                     = var.region
  service_account_email      = var.service_account_email
  subnetwork_name            = module.vpc.subnets["us-east1/manager"].self_link
  vpc                        = module.vpc.network_self_link
  network_tags               = var.manager_network_tags
  chef_provision             = var.chef_provision
  startup_script             = module.vfiles.bootstrap.content
  chef_prov_script_path      = module.vfiles.chefprov.filename
  chef_bootstrap_script_path = module.vfiles.chefbootstrap.filename
  chef_deprov_script_path    = module.vfiles.chefdeprov.filename
  chef_delete_script_path    = module.vfiles.chefdelete.filename
  sleep_time                 = var.sleep_time
  additional_labels          = var.manager_labels
}

###############################################
#
# Load balancer and VM for the org-ci bastion
#
###############################################

resource "google_service_account" "bastion" {
  account_id   = "bastion"
  display_name = "bastion Account"
  description  = "Service account used by the bastion server. It should have minimal privileges."
}

resource "google_project_iam_custom_role" "bastion" {
  role_id     = "bastion"
  title       = "Bastion Role"
  description = "Compute service account for bastion servers"
  permissions = var.bastion_role_permissions
}

resource "google_project_iam_member" "bastion" {
  role   = google_project_iam_custom_role.bastion.id
  member = "serviceAccount:${google_service_account.bastion.email}"
}

module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

module "bastion" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = google_service_account.bastion.email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v5.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.vpc.network_self_link
}

###############################################
#
# GKE Cluster for Prometheus
#
###############################################

# Service account utilized by CI
resource "google_service_account" "k8s-workloads" {
  account_id   = "k8s-workloads"
  display_name = "k8s-workloads"
}

# Service account utilized by CI for Read Only operations
resource "google_service_account" "k8s-workloads-ro" {
  account_id   = "k8s-workloads-ro"
  display_name = "k8s-workloads-ro"
}

resource "google_project_iam_custom_role" "k8s-workloads" {
  project     = var.project
  role_id     = "k8sWorkloads"
  title       = "k8s-workloads"
  permissions = ["clientauthconfig.clients.listWithSecrets"]
}

resource "google_project_iam_binding" "k8s-workloads" {
  project = var.project
  role    = "projects/${var.project}/roles/${google_project_iam_custom_role.k8s-workloads.role_id}"

  members = [
    "serviceAccount:${google_service_account.k8s-workloads.email}",
    "serviceAccount:${google_service_account.k8s-workloads-ro.email}"
  ]
}

# Reserved IP address for prometheus operator
resource "google_compute_address" "prometheus-gke" {
  name         = "prometheus-gke-${var.environment}"
  description  = "prometheus-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

module "gitlab-gke" {
  name        = "gitlab-gke"
  environment = var.environment
  vpc         = module.vpc.network_self_link
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v9.0.1"

  authorized_master_access = [
    "35.231.50.113/32",  # runner-release-01-inf-ops
    "104.196.173.63/32", # bastion-01-inf-org-ci
  ]

  ip_cidr_range          = var.subnetworks["gitlab-gke"]
  disable_network_policy = "false"
  dns_zone_name          = var.dns_zone_name
  kubernetes_version     = "1.16.8-gke.15"
  node_network_policy    = "true"
  private_cluster        = "true"
  private_master_cidr    = var.master_cidr_subnets["gitlab-gke"]
  project                = var.project
  region                 = var.region
  pod_ip_cidr_range      = var.subnetworks["gitlab-gke-pod-cidr"]
  service_ip_cidr_range  = var.subnetworks["gitlab-gke-service-cidr"]

  service_account = "default"

  node_pools = {
    "default-20200601-0" = {
      initial_node_count = "1"
      machine_type       = var.machine_types["gitlab-gke"]
      max_node_count     = "1"
      node_auto_repair   = "true"
      node_auto_upgrade  = "false"
      node_disk_size_gb  = "100"
      node_disk_type     = "pd-ssd"
      preemptible        = "false"
      type               = "default"
    }
  }
}

// Allow GKE to talk to the prometheus operator which utilizes port 8443
resource "google_compute_firewall" "gke-master-to-kubelet" {
  name    = "k8s-api-to-kubelets"
  network = module.vpc.network_self_link
  project = var.project

  description = "GKE API to kubelets"

  source_ranges = [var.master_cidr_subnets["gitlab-gke"]]

  allow {
    protocol = "tcp"
    ports    = ["8443"]
  }

  target_tags = ["gitlab-gke"]
}

# Reserved IP address for prometheus ingress with IAP
resource "google_compute_global_address" "prometheus-ingress-iap" {
  name        = "prometheus-ingress-iap-${var.environment}"
  description = "prometheus-ingress-iap-${var.environment}"
}

module "prometheus-gke-pre-gitlab-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "prometheus-gke.org-ci.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.prometheus-ingress-iap.address]
    }
  }
}

# IP address for NAT
resource "google_compute_address" "gke-cloud-nat-ip" {
  name        = "gitlab-gke"
  description = "gitlab-gke"
}

resource "google_compute_router" "nat-router" {
  name    = "gitlab-gke"
  network = module.vpc.network_self_link
}

resource "google_compute_router_nat" "gke-nat" {
  name                               = "gitlab-gke"
  router                             = google_compute_router.nat-router.name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.gke-cloud-nat-ip.self_link]
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = module.gitlab-gke.subnetwork_self_link
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}
