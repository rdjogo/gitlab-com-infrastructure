variable "project" {
  default = "gitlab-release"
}

variable "bootstrap_script_version" {
  default = 8
}

variable "region" {
  default = "us-east1"
}

variable "environment" {
  default = "release"
}

variable "dns_zone_name" {
  default = "gitlab.net"
}

variable "machine_types" {
  type = map(string)

  default = {
    "all-in-one" = "custom-8-16384"
  }
}

variable "service_account_email" {
  type = string

  default = "terraform@gitlab-release.iam.gserviceaccount.com"
}

#############################
# Default firewall
# rule for allowing
# all protocols on all
# ports
#
# 10.232.x.x: all of pre
# 10.250.7.x: ops runner
# 10.250.10.x: chatops runner
# 10.250.12.x: release runner
# 10.12.0.0/14: pod address range in gitlab-ops for runners
###########################

variable "internal_subnets" {
  type    = list(string)
  default = ["10.232.0.0/13", "10.250.7.0/24", "10.250.10.0/24", "10.250.12.0/24", "10.12.0.0/14"]
}

##################
# Network Peering
##################

variable "network_env" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-release/global/networks/release"
}

variable "peer_networks" {
  type = map(list(string))

  default = {
    "names" = ["ops"]
    "links" = [
      "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops",
    ]
  }
}

variable "subnetworks" {
  type = map(string)

  default = {
    "all-in-one" = "10.208.0.0/24"
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "all-in-one" = [22, 80, 443]
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "all-in-one" = 1
  }
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-release-chef-bootstrap"
    bootstrap_key     = "gitlab-release-bootstrap-validation"
    bootstrap_keyring = "gitlab-release-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-release.iam.gserviceaccount.com"
}

variable "egress_ports" {
  type    = list(string)
  default = ["80", "443"]
}

### Object Storage Configuration

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

variable "cloudflare_zone_name" {}

variable "cloudflare_zone_id" {}

variable "cloudflare_api_key" {}

variable "cloudflare_account_id" {}

variable "cloudflare_email" {}
