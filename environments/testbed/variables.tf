variable "gitlab_io_zone_id" {
}

variable "project" {
  default = "gitlab-testbed"
}

variable "bootstrap_script_version" {
  default = 8
}

variable "region" {
  default = "us-east1"
}

variable "environment" {
  default = "testbed"
}

variable "dns_zone_name" {
  default = "gitlab.net"
}

variable "oauth2_client_id_monitoring" {
  default = "test"
}

variable "oauth2_client_secret_monitoring" {
  default = "test"
}

variable "machine_types" {
  type = map(string)

  default = {
    "bastion"       = "n1-standard-1"
    "web"           = "n1-standard-16"
    "monitoring"    = "n1-standard-2"
    "gitlab-onprem" = "n1-standard-2"
    "sd-exporter"   = "n1-standard-1"
    "gke-runner"    = "n1-standard-2"
    "gitaly"        = "n1-standard-2"
  }
}

variable "monitoring_hosts" {
  type = map(list(string))

  default = {
    "names" = ["prometheus", "prometheus-app"]
    "ports" = [9090, 9090]
  }
}

variable "service_account_email" {
  type = string

  default = "terraform@gitlab-testbed.iam.gserviceaccount.com"
}

#############################
# Default firewall
# rule for allowing
# all protocols on all
# ports
#
# 10.240.x.x: all of testbed
# 10.250.7.x: ops runner
# 10.250.10.x: chatops runner
# 10.250.12.x: release runner
# 10.12.0.0/14: pod address range in gitlab-ops for runners
###########################

variable "internal_subnets" {
  type    = list(string)
  default = ["10.240.0.0/13", "10.250.7.0/24", "10.250.10.0/24", "10.250.12.0/24", "10.12.0.0/14"]
}

variable "other_monitoring_subnets" {
  type = list(string)

  # Left empty for testbed
  default = []
}

# The testbed network is allocated
# 10.240.0.0/13
#   First IP: 10.240.0.0
#   Last IP:  10.247.255.255
# For allocations by project see https://gitlab.com/gitlab-com/docs/uncategorized/blob/master/howto/subnet-allocations.md

variable "subnetworks" {
  type = map(string)

  default = {
    "bastion"                 = "10.240.1.0/24"
    "monitoring"              = "10.240.3.0/24"
    "sd-exporter"             = "10.240.6.0/24"
    "redis"                   = "10.240.7.0/24"
    "gitlab-onprem"           = "10.240.8.0/24"
    "gke-runner"              = "10.240.9.0/24"
    "gitaly"                  = "10.240.10.0/24"
    "filestore-storage"       = "10.240.128.0/29"
    "gke-runner-pod-cidr"     = "10.246.0.0/16"
    "gke-runner-service-cidr" = "10.247.0.0/16"
  }
}

##################
# Network Peering
##################

variable "network_env" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-testbed/global/networks/testbed"
}

variable "peer_networks" {
  type = map(list(string))

  default = {
    "names" = ["ops"]
    "links" = [
      "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops",
    ]
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "bastion"       = [22]
    "gitlab-onprem" = [443, 80, 22, 5005]
    "sd-exporter"   = []
    "gitaly"        = []
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "bastion"     = 1
    "web"         = 1
    "prometheus"  = 1
    "sd-exporter" = 1
    "gitaly"      = 1
  }
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-testbed-chef-bootstrap"
    bootstrap_key     = "gitlab-testbed-bootstrap-validation"
    bootstrap_keyring = "gitlab-testbed-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "data_disk_sizes" {
  type = map(string)

  default = {
    "web" = "100"
  }
}

variable "lb_fqdns" {
  type    = list(string)
  default = ["onprem.testbed.gitlab.net"]
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.testbed.gitlab.com"]
}

variable "lb_fqdns_registry" {
  type    = list(string)
  default = ["registry.onprem.testbed.gitlab.net"]
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "tcp_lbs" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https", "ssh", "registry"]
    "forwarding_port_ranges" = ["80", "443", "22", "5000"]
    "health_check_ports"     = ["8001", "8002", "8003", "5000"] #Is the healthcheck for registry on 5000, or do we need 8004  here?
  }
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-testbed.iam.gserviceaccount.com"
}

variable "egress_ports" {
  type    = list(string)
  default = ["80", "443"]
}

### Object Storage Configuration

variable "artifact_age" {
  type    = string
  default = "30"
}

variable "upload_age" {
  type    = string
  default = "30"
}

variable "lfs_object_age" {
  type    = string
  default = "30"
}

variable "package_repo_age" {
  type    = string
  default = "30"
}

variable "storage_class" {
  type    = string
  default = "MULTI_REGIONAL"
}

variable "storage_log_age" {
  type    = string
  default = "7"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

#################
# Monitoring whitelist
#################

#################
# Allow traffic from the ops
# network from grafana
#################

variable "monitoring_whitelist_prometheus" {
  type = map(list(string))

  default = {
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    # 10.250.8.x for the ops prometheus servers
    #
    # Port 9090 is for prometheus
    # Ports 10900-10902 is for thanos
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]
    "ports"   = ["9090", "10900", "10901", "10902"]
  }
}

variable "monitoring_whitelist_thanos" {
  type = map(list(string))

  default = {
    # 10.250.8.x for the ops prometheus servers
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]
    "ports"   = ["10901", "10902"]
  }
}

#################
# Allow traffic from the ops
# network from the alerts manager
#################
variable "monitoring_whitelist_alerts" {
  type = map(list(string))

  default = {
    # 10.250.8.x for the ops alerts servers
    "subnets" = ["10.250.8.0/24"]
    "ports"   = ["9093"]
  }
}

####################################
# Default log filters for stackdriver
#####################################

variable "sd_log_filters" {
  type = map(string)

  default = {
    "exclude_logtypes" = "resource.type=\"gce_instance\" AND labels.tag!=\"haproxy\""
  }
}

variable "cloudflare_api_key" {}
variable "cloudflare_account_id" {}
variable "cloudflare_email" {}
