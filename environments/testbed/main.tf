## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/testbed/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

## Local
provider "local" {
  version = "~> 1.4.0"
}

variable "gitlab_com_zone_id" {
}

variable "gitlab_net_zone_id" {
}

## Google

provider "google" {
  version = "~> 2.20.0"
  project = var.project
  region  = var.region
}

provider "google-beta" {
  version = "~> 3.22.0"
  project = var.project
  region  = var.region
}

## CloudFlare

provider "cloudflare" {
  version    = "= 2.3"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

##################################
#
#  Network
#
#################################

module "network" {
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.0"
  project          = var.project
  environment      = var.environment
  internal_subnets = var.internal_subnets
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering" {
  count        = length(var.peer_networks["names"])
  name         = "peering-${element(var.peer_networks["names"], count.index)}"
  network      = var.network_env
  peer_network = element(var.peer_networks["links"], count.index)
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = var.subnetworks["monitoring"]
  name                     = format("monitoring-%v", var.environment)
  network                  = module.network.self_link
  private_ip_google_access = true
  project                  = var.project
  region                   = var.region
}

###############################################
#
# Load balancer and VM for the pre bastion
#
###############################################

module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

module "bastion" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Google storage buckets
#
##################################

module "storage" {
  environment                       = var.environment
  artifact_age                      = var.artifact_age
  lfs_object_age                    = var.lfs_object_age
  package_repo_age                  = var.package_repo_age
  upload_age                        = var.upload_age
  storage_log_age                   = var.storage_log_age
  storage_class                     = var.storage_class
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.0.0"
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  environment     = var.environment
  dns_zone_id     = var.gitlab_net_zone_id
  dns_zone_name   = "gitlab.net"
  dns_zone_prefix = "testbed."
  hosts           = var.monitoring_hosts["names"]
  name            = "monitoring-lb"
  project         = var.project
  region          = var.region
  service_ports   = var.monitoring_hosts["ports"]
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v3.0.1"
  subnetwork_name = google_compute_subnetwork.monitoring.name
  targets         = var.monitoring_hosts["names"]
  url_map         = google_compute_url_map.monitoring-lb.self_link
}

#######################
module "prometheus" {
  attach_data_disk  = true
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size    = 100
  data_disk_type    = "pd-standard"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus"
  node_count            = var.node_count["prometheus"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  public_ports          = ["22"]
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/graph"
  service_port = element(
    var.monitoring_hosts["ports"],
    index(var.monitoring_hosts["names"], "prometheus"),
  )
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v4.2.1"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

#######################################################
#
# VM for onprem.testbed.gitlab.net
#
#######################################################

module "onprem-testbed-gitlab-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "onprem.testbed.gitlab.net." = {
      ttl     = "300"
      records = module.gitlab-onprem.instances.*.network_interface.0.access_config.0.nat_ip
    }
  }
}

module "gitlab-onprem" {
  backend_protocol         = "HTTPS"
  bootstrap_version        = var.bootstrap_script_version
  chef_provision           = var.chef_provision
  chef_run_list            = "\"role[${var.environment}-base-gitlab-onprem]\""
  data_disk_size           = 1000
  data_disk_type           = "pd-ssd"
  dns_zone_name            = var.dns_zone_name
  environment              = var.environment
  external_backend_service = "NONE"
  health_check             = "http"
  health_check_port        = 8887
  ip_cidr_range            = var.subnetworks["gitlab-onprem"]
  machine_type             = var.machine_types["gitlab-onprem"]
  name                     = "gitlab-onprem"
  node_count               = 1
  persistent_disk_path     = "/var/opt/gitlab"
  project                  = var.project
  public_ports             = var.public_ports["gitlab-onprem"]
  region                   = var.region
  service_account_email    = var.service_account_email
  service_path             = "/-/liveness"
  service_port             = 443
  source                   = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v4.2.1"
  tier                     = "inf"
  use_external_ip          = true
  use_new_node_name        = true
  vpc                      = module.network.self_link
}

module "sd-exporter" {
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  machine_type              = var.machine_types["sd-exporter"]
  name                      = "sd-exporter"
  node_count                = var.node_count["sd-exporter"]
  project                   = var.project
  public_ports              = var.public_ports["sd-exporter"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name           = google_compute_subnetwork.monitoring.name
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

##################################
#
#  Gitaly node
#
##################################

module "gitaly" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-be-gitaly]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["gitaly"]
  machine_type          = var.machine_types["gitaly"]
  name                  = "gitaly"
  node_count            = var.node_count["gitaly"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["gitaly"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  GKE Cluster for runners
#
##################################

# After provisioning you will need to configure
# the cluster for gitlab-runner. Instructions
# for this are https://gitlab.com/gitlab-com/runbooks/tree/master/docs/uncategorized/gke-runner

module "gke-runner" {
  environment = var.environment
  name        = "gke-runner"
  vpc         = module.network.self_link
  # TODO: migrate to v6.0.0+: https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7861
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=craigf/v1.0.0-tf0.12"
  initial_node_count    = 1
  ip_cidr_range         = var.subnetworks["gke-runner"]
  dns_zone_name         = var.dns_zone_name
  machine_type          = var.machine_types["gke-runner"]
  project               = var.project
  region                = var.region
  pod_ip_cidr_range     = var.subnetworks["gke-runner-pod-cidr"]
  service_ip_cidr_range = var.subnetworks["gke-runner-service-cidr"]
  kubernetes_version    = "1.13.12-gke.25"
}
