variable "sre_group_name" {
  type    = string
  default = "Gitlab - SRE"
}

variable "githost_support_group_name" {
  type    = string
  default = "Gitlab - GitHost Support"
}

variable "backend_engineers_group_name" {
  type    = string
  default = "GitLab - Backend Engineers"
}

variable "qa_tunnel_group_name" {
  type    = string
  default = "GitLab - ASA - QA Tunnel"
}
