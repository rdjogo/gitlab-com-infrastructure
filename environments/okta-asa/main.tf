## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/okta-asa/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "scaleft" {
}

##############################
# Windows CI project
##############################
resource "scaleft_project" "windows_ci" {
  project_name = "windows_ci"
}

resource "scaleft_enrollment_token" "windows_ci" {
  project_name = scaleft_project.windows_ci.project_name
  description = "windows_ci"
}

resource "scaleft_assign_group" "sres_to_windows_ci" {
  project_name = scaleft_project.windows_ci.project_name
  group_name = var.sre_group_name
  server_access = true
  server_admin = true
  create_server_group = false
}

##############################
# Githost project
##############################
resource "scaleft_project" "githost" {
  project_name = "githost"
}

resource "scaleft_enrollment_token" "githost" {
  project_name = scaleft_project.githost.project_name
  description = "GitHost Servers"
}

resource "scaleft_assign_group" "githost_support_to_githost" {
  project_name = scaleft_project.githost.project_name
  group_name = var.githost_support_group_name
  server_access = true
  server_admin = true
  create_server_group = true
}

##############################
# QA Tunnel project
##############################
resource "scaleft_project" "qa_tunnel" {
  project_name = "qa_tunnel"
}

resource "scaleft_enrollment_token" "qa_tunnel" {
  project_name = scaleft_project.qa_tunnel.project_name
  description = "QA Tunnel Token"
}

resource "scaleft_assign_group" "backend_engineers_to_qa_tunnel" {
  project_name = scaleft_project.qa_tunnel.project_name
  group_name = var.backend_engineers_group_name
  server_access = true
  server_admin = false
  create_server_group = true
}

resource "scaleft_assign_group" "qa_tunnel_to_qa_tunnel" {
  project_name = scaleft_project.qa_tunnel.project_name
  group_name = var.qa_tunnel_group_name
  server_access = true
  server_admin = false
  create_server_group = true
}

##############################
# Gitlab-ops project
##############################
resource "scaleft_project" "gitlab_ops" {
  project_name = "gitlab_ops"
}

resource "scaleft_enrollment_token" "gitlab_ops" {
  project_name = scaleft_project.gitlab_ops.project_name
  description = "Gitlab Ops Token"
}

resource "scaleft_assign_group" "sre_to_gitlab_ops" {
  project_name = scaleft_project.gitlab_ops.project_name
  group_name = var.sre_group_name
  server_access = true
  server_admin = true
  create_server_group = true
}
