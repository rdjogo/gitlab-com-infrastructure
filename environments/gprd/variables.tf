variable "oauth2_client_id_monitoring" {

}

variable "oauth2_client_secret_monitoring" {
}

variable "gitlab_net_zone_id" {}

variable "gitlab_com_zone_id" {}

variable "gitlab_io_zone_id" {}

variable "gitlab_static_net_zone_id" {}

variable "static_objects_cache_sentry_key" {}
variable "static_objects_external_storage_token" {}
variable "static_objects_cache_elasticsearch_auth" {}

variable "cloudflare_account_id" {}
variable "cloudflare_api_key" {}
variable "cloudflare_email" {}
variable "cloudflare_zone_name" {}
variable "cloudflare_zone_id" {}

variable "cloudflare_net_zone_id" {}

variable "bootstrap_script_version" {
  default = 8
}

#############################
# Default firewall
# rule for allowing
# all protocols on all
# ports
#
# 10.216.x.x: all of gprd
# 10.250.7.x: ops runner
# 10.250.8.11/32: nessus scanner
# 10.250.10.x: chatops runner
# 10.250.12.x: release runner
# 10.12.0.0/14: pod address range in gitlab-ops for runners
###########################

variable "internal_subnets" {
  type    = list(string)
  default = ["10.216.0.0/13", "10.250.7.0/24", "10.250.8.11/32", "10.250.10.0/24", "10.250.12.0/24", "10.12.0.0/14"]
}

variable "other_monitoring_subnets" {
  type = list(string)

  # 10.226.1.0/24: gstg
  # 10.251.17.0/24: dr
  default = ["10.226.1.0/24", "10.251.17.0/24"]
}

variable "monitoring_hosts" {
  type = map(list(string))

  default = {
    "names" = ["alerts", "prometheus", "prometheus-app", "prometheus-db"]
    "ports" = [9093, 9090, 9090, 9090]
  }
}

#### GCP load balancing

# The top level domain record for the GitLab deployment.
# For production this should be set to "gitlab.com"
# Note: Currently `gitlab.com` is set outside of terraform
#       because of the switchover.

variable "lb_fqdns" {
  type    = list(string)
  default = ["gitlab.com"]
}

##########
variable "lb_fqdns_altssh" {
  type    = list(string)
  default = ["altssh.gitlab.com"]
}

variable "lb_fqdns_registry" {
  type    = list(string)
  default = ["registry.gitlab.com"]
}

variable "lb_fqdns_cny" {
  type    = list(string)
  default = []
}

variable "lb_fqdns_pages" {
  type    = list(string)
  default = ["*.pages.gprd.gitlab.io"]
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.gprd.gitlab.com"]
}

variable "lb_fqdns_internal" {
  type    = list(string)
  default = ["int.gprd.gitlab.net"]
}

variable "lb_fqdns_internal_praefect" {
  type    = list(string)
  default = []
}

variable "lb_fqdns_internal_pgbouncer" {
  type    = list(string)
  default = ["pgbouncer.int.gprd.gitlab.net"]
}

variable "lb_fqdns_internal_pgbouncer_sidekiq" {
  type    = list(string)
  default = ["pgbouncer-sidekiq.int.gprd.gitlab.net"]
}

variable "lb_fqdns_internal_patroni" {
  type    = list(string)
  default = ["patroni.int.gprd.gitlab.net"]
}

#
# For every name there must be a corresponding
# forwarding port range and health check port
#

variable "tcp_lbs" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https", "ssh"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "tcp_lbs_internal" {
  type = map(list(string))

  default = {
    "names"                  = ["http-internal", "https-internal", "alt-https-internal", "ssh-internal"]
    "forwarding_port_ranges" = ["80", "443", "11443", "22"]
    "health_check_ports"     = ["8001", "8002", "8002", "8003"]
  }
}

variable "tcp_lbs_internal_praefect" {
  type = map(list(string))

  default = {
    "names"                      = ["praefect"]
    "forwarding_port_ranges"     = ["2305"]
    "health_check_ports"         = ["9652"]
    "health_check_request_paths" = ["/metrics"]
  }
}

variable "tcp_lbs_pages" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_altssh" {
  type = map(list(string))

  default = {
    "names"                      = ["https"]
    "forwarding_port_ranges"     = ["443"]
    "health_check_ports"         = ["8003"]
    "health_check_request_paths" = ["/-/available-ssh"]
  }
}

variable "tcp_lbs_registry" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_cny" {
  type = map(list(string))

  default = {
    "names"                  = []
    "forwarding_port_ranges" = []
    "health_check_ports"     = []
  }
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

##################
# Network Peering
##################

variable "network_env" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-production/global/networks/gprd"
}

variable "peer_networks" {
  type = map(list(string))

  default = {
    "names" = ["ops", "gstg", "vault"]
    "links" = [
      "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops",
      "https://www.googleapis.com/compute/v1/projects/gitlab-staging-1/global/networks/gstg",
      "https://www.googleapis.com/compute/v1/projects/gitlab-vault/global/networks/vault",
    ]
  }
}

######################

variable "base_chef_run_list" {
  default = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
}

variable "empty_chef_run_list" {
  default = "\"\""
}

variable "dns_zone_name" {
  default = "gitlab.com"
}

variable "run_lists" {
  type = map(string)

  default = {
    "prometheus"  = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
    "performance" = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "api"           = []
    "bastion"       = [22]
    "blackbox"      = []
    "consul"        = []
    "console"       = []
    "deploy"        = []
    "runner"        = []
    "db-dr"         = []
    "pgbouncer"     = []
    "fe-lb"         = [22, 80, 443]
    "git"           = []
    "patroni"       = []
    "praefect"      = []
    "pubsubbeat"    = []
    "redis"         = []
    "redis-sidekiq" = []
    "redis-cache"   = []
    "sidekiq"       = []
    "sd-exporter"   = []
    "stor"          = []
    "thanos"        = []
    "web"           = []
    "web-pages"     = []
    "monitoring"    = []
  }
}

variable "environment" {
  default = "gprd"
}

variable "format_data_disk" {
  default = "true"
}

variable "project" {
  default = "gitlab-production"
}

variable "region" {
  default = "us-east1"
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-gprd-chef-bootstrap"
    bootstrap_key     = "gitlab-gprd-bootstrap-validation"
    bootstrap_keyring = "gitlab-gprd-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "monitoring_cert_link" {
  default = "projects/gitlab-production/global/sslCertificates/wildcard-gprd-gitlab-net-exp-2021-05-14"
}

variable "data_disk_sizes" {
  type = map(string)

  default = {
    "file"          = "16000"
    "file-cny"      = "4000"
    "file-marquee"  = "4000"
    "file-praefect" = "16000"
    "file-hdd"      = "16000"
    "share"         = "20000"
    "pages"         = "16000"
    "patroni"       = "10000"
    "prometheus"    = "4000"
    "prometheus-db" = "2000"
  }
}

variable "machine_types" {
  type = map(string)

  default = {
    "alerts"                           = "n1-standard-1"
    "api"                              = "c2-standard-16"
    "api-cny"                          = "n1-standard-16"
    "bastion"                          = "g1-small"
    "blackbox"                         = "n1-standard-1"
    "camoproxy"                        = "n1-standard-1"
    "consul"                           = "n1-standard-4"
    "db-dr-archive"                    = "n1-standard-16"
    "db-dr-delayed"                    = "n1-standard-8"
    "console"                          = "n1-standard-4"
    "deploy"                           = "n1-standard-2"
    "fe-lb"                            = "c2-standard-8"
    "fe-lb-pages"                      = "c2-standard-8"
    "fe-lb-altssh"                     = "n1-standard-4"
    "fe-lb-registry"                   = "n1-standard-4"
    "git"                              = "custom-16-20480"
    "gitlab-gke"                       = "n1-standard-8"
    "gitlab-gke-low-urgency-cpu-bound" = "c2-standard-4"
    "gitlab-gke-urgent-other"          = "n1-standard-32"
    "gitlab-gke-urgent-cpu-bound"      = "c2-standard-4"
    "influxdb"                         = "n1-standard-8"
    "monitoring-app"                   = "n1-highmem-8"
    "monitoring-db"                    = "n1-highmem-8"
    "monitoring-default"               = "n1-highmem-8"
    "patroni"                          = "n1-highmem-96"
    "praefect"                         = "n1-standard-16"
    "praefect-db"                      = "db-custom-8-30720"
    "pgbouncer"                        = "n1-standard-4"
    "redis"                            = "c2-standard-8"
    "redis-sidekiq"                    = "c2-standard-4"
    "redis-cache"                      = "c2-standard-30"
    "redis-cache-sentinel"             = "n1-standard-1"
    "runner"                           = "n1-standard-2"
    "sd-exporter"                      = "n1-standard-1"
    # Important note: when changing the number of cores on sidekiq workers,
    # please be sure to reconfigure the respective sidekiqProcessCount in
    # https://ops.gitlab.net/gitlab-cookbooks/chef-repo/blob/master/tools/sidekiq-config/sidekiq-queue-configurations.libsonnet
    "sidekiq-elasticsearch"         = "n1-standard-2"
    "sidekiq-low-urgency-cpu-bound" = "n1-standard-2"
    "sidekiq-catchall"              = "n1-standard-8"
    "sidekiq-urgent-other"          = "n1-standard-8"
    "sidekiq-urgent-cpu-bound"      = "c2-standard-4"
    "stor"                          = "n1-standard-32"
    "stor-cny"                      = "n1-standard-32"
    "stor-marquee"                  = "n1-standard-32"
    "stor-praefect"                 = "n1-standard-32"
    "stor-hdd"                      = "n1-standard-8"
    "thanos-compact"                = "n1-highmem-2"
    "thanos-store"                  = "n1-highmem-4"
    "thanos-store-app"              = "n1-highmem-4"
    "thanos-store-db"               = "n1-highmem-4"
    "web"                           = "c2-standard-16"
    "web-pages"                     = "n1-standard-8"
    "stor-pages"                    = "n1-highmem-8"
    "stor-share"                    = "n1-highmem-8"
  }
  # pages and share should eventually be upgraded
  # to n1-standard-32 for better IO.
}

variable "node_count" {
  type = map(string)

  default = {
    "api"                           = 18
    "bastion"                       = 3
    "blackbox"                      = 1
    "camoproxy"                     = 2
    "console"                       = 1
    "consul"                        = 5
    "db-dr"                         = 2
    "deploy"                        = 1
    "deploy-cny"                    = 1
    "fe-lb"                         = 12
    "fe-lb-altssh"                  = 3
    "fe-lb-pages"                   = 5
    "fe-lb-registry"                = 2
    "fe-lb-registry-temp"           = 4
    "fe-lb-cny"                     = 0
    "git"                           = 24
    "patroni"                       = 8
    "pages"                         = 1
    "pgbouncer"                     = 3
    "pgbouncer-sidekiq"             = 3
    "redis"                         = 3
    "redis-sidekiq"                 = 3
    "redis-cache"                   = 3
    "redis-cache-sentinel"          = 3
    "runner"                        = 1
    "share"                         = 1
    "sd-exporter"                   = 1
    "sidekiq-elasticsearch"         = 0
    "sidekiq-low-urgency-cpu-bound" = 0
    "sidekiq-catchall"              = 7
    "sidekiq-urgent-other"          = 0
    "sidekiq-urgent-cpu-bound"      = 0
    "stor"                          = 20
    "stor-cny"                      = 1
    "stor-marquee"                  = 3
    "stor-praefect"                 = 2
    "stor-hdd"                      = 1
    "thanos-compact"                = 1
    "thanos-store"                  = 2
    "thanos-store-app"              = 2
    "thanos-store-db"               = 2
    "multizone-stor"                = 34
    "web"                           = 24
    "web-pages"                     = 8
    "web-cny"                       = 6
    "api-cny"                       = 2
    "git-cny"                       = 2
    "alerts"                        = 2
    "prometheus"                    = 2
    "prometheus-app"                = 2
    "prometheus-db"                 = 2
    "praefect"                      = 3
  }
}

variable "subnetworks" {
  type = map(string)

  default = {
    "fe-lb"                   = "10.216.1.0/24"
    "fe-lb-pages"             = "10.216.2.0/24"
    "fe-lb-altssh"            = "10.216.3.0/24"
    "bastion"                 = "10.216.4.0/24"
    "fe-lb-registry"          = "10.216.5.0/24"
    "fe-lb-registry-temp"     = "10.221.13.0/24"
    "fe-lb-cny"               = "10.216.6.0/24"
    "gitlab-gke"              = "10.216.8.0/23" # ~512 nodes
    "redis"                   = "10.217.2.0/24"
    "db-dr-delayed"           = "10.217.3.0/24"
    "db-dr-archive"           = "10.217.7.0/24"
    "patroni"                 = "10.220.16.0/24"
    "pgbouncer"               = "10.217.4.0/24"
    "pgbouncer-sidekiq"       = "10.217.8.0/24"
    "redis-cache"             = "10.217.5.0/24"
    "redis-sidekiq"           = "10.217.6.0/24"
    "consul"                  = "10.218.1.0/24"
    "deploy"                  = "10.218.3.0/24"
    "runner"                  = "10.218.4.0/24"
    "console"                 = "10.218.5.0/24"
    "deploy-cny"              = "10.218.7.0/24"
    "monitoring"              = "10.219.1.0/24"
    "pubsubbeat-duplicate"    = "10.219.4.0/24"
    "api"                     = "10.220.2.0/23"
    "git"                     = "10.220.4.0/23"
    "sidekiq"                 = "10.220.6.0/23"
    "thanos-compact"          = "10.220.18.0/24"
    "thanos-store"            = "10.220.17.0/24"
    "web"                     = "10.220.8.0/23"
    "web-pages"               = "10.220.12.0/23"
    "camoproxy"               = "10.220.19.0/24"
    "stor"                    = "10.221.2.0/23"
    "gitlab-gke-service-cidr" = "10.221.4.0/23" # ~512 services
    "gitlab-gke-pod-cidr"     = "10.222.0.0/16" # ~65k pods
    "pages"                   = "10.221.6.0/24"
    "share"                   = "10.221.7.0/24"
    "stor-cny"                = "10.221.8.0/24"
    "stor-marquee"            = "10.221.9.0/24"
    "stor-praefect"           = "10.221.11.0/24"
    "stor-hdd"                = "10.221.12.0/24"
    "praefect"                = "10.221.10.0/24"
  }
  ###############################
  # These will eventually (tm) be
  # moved to object storage

  #############################
}

variable "master_cidr_subnets" {
  type = map(string)

  default = {
    "gitlab-gke" = "172.16.0.0/28"
  }
}

variable "service_account_email" {
  type = string

  default = "terraform@gitlab-production.iam.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage-prd@gitlab-production.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_service_account" {
  type    = string
  default = "postgres-wal-archive@gitlab-production.iam.gserviceaccount.com"
}

# Service account used to do automated backup testing
# in https://gitlab.com/gitlab-restore/postgres-gprd
variable "gcs_postgres_restore_service_account" {
  type    = string
  default = "postgres-automated-backup-test@gitlab-restore.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_kms_key_id" {
  type    = string
  default = "projects/gitlab-production/locations/global/keyRings/gitlab-secrets/cryptoKeys/gprd-postgres-wal-archive"
}

variable "egress_ports" {
  type    = list(string)
  default = []
}

variable "web_egress_ports" {
  type    = list(string)
  default = []
}

variable "deploy_egress_ports" {
  type    = list(string)
  default = []
}

variable "console_egress_ports" {
  type    = list(string)
  default = []
}

variable "os_boot_image" {
  type = map(string)

  default = {
    "camoproxy"   = "ubuntu-os-cloud/ubuntu-1804-lts"
    "fe-lb"       = "ubuntu-os-cloud/ubuntu-1804-lts"
    "fe-lb-pages" = "ubuntu-os-cloud/ubuntu-1804-lts"
  }
}

####################################
# Camo proxy values
#####################################

variable "camoproxy_cert_link" {
  default = "projects/gitlab-production/global/sslCertificates/user-content-gitlab-static-net"
}

variable "camoproxy_domain" {
  type    = string
  default = "gitlab-static.net"
}

variable "camoproxy_serviceport" {
  type    = string
  default = 80 # Actually haproxy, or whatever is in front of camoproxy doing blacklisting
}

variable "camoproxy_hostname" {
  type    = string
  default = "user-content"
}

variable "camoproxy_tmp_hostname" {
  type    = string
  default = "user-content-tmp"
}

# This is permanent, regardless of any other egress port changes.
variable "camoproxy_egress_ports" {
  type    = list(string)
  default = ["80", "443"]
}

#######################
# pubsubbeat config
#######################

variable "pubsubbeats-duplicates" {
  type = map(list(string))

  default = {
    "names" = [
      "gitaly",
      "haproxy",
      "pages",
      "postgres",
      "production",
      "system",
      "workhorse",
      "rspec",
      "sidekiq",
      "api",
      "nginx",
      "gitlab-shell",
      "shell",
      "rails",
      "unstructured",
      "unicorn",
      "application",
      "registry",
      "redis",
      "consul",
      "runner",
      "camoproxy",
      "gke",
      "monitoring",
      "praefect",
      "puma",
    ]
    "machine_types" = [
      "n1-standard-4",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-4",
      "n1-standard-4",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-4",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-8",
      "n1-standard-2",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
    ]
  }
}

### Object Storage Configuration

variable "versioning" {
  type    = string
  default = "true"
}

variable "artifact_age" {
  type    = string
  default = "30"
}

variable "upload_age" {
  type    = string
  default = "30"
}

variable "lfs_object_age" {
  type    = string
  default = "30"
}

variable "package_repo_age" {
  type    = string
  default = "30"
}

variable "storage_class" {
  type    = string
  default = "MULTI_REGIONAL"
}

variable "storage_log_age" {
  type    = string
  default = "7"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

#################
# Monitoring whitelist
#################

#################
# Allow traffic from the ops
# network from grafana
#################

variable "monitoring_whitelist_prometheus" {
  type = map(list(string))

  default = {
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    # 10.250.8.x for the ops prometheus servers
    #
    # Port 9090 is for prometheus
    # Ports 10900-10902 is for thanos
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]
    "ports"   = ["9090", "10900", "10901", "10902"]
  }
}

variable "monitoring_whitelist_thanos" {
  type = map(list(string))

  default = {
    # 10.250.8.x for the ops prometheus servers
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]
    "ports"   = ["10901", "10902"]
  }
}

#################
# Allow alertmanager gossip traffic from the ops
# network from the alerts manager
#################
variable "monitoring_whitelist_alertmanagers" {
  type = map(list(string))

  default = {
    # 10.250.8.x for the ops alertmanager servers
    "subnets" = ["10.250.8.0/24"]
    "ports"   = ["9094"]
  }
}

#################
# Allow Prometheus to alertmanager traffic from other environments
#################
variable "monitoring_whitelist_alerts" {
  type = map(list(string))

  default = {
    # 10.251.17.0/24 for dr prometheus servers
    # 10.226.1.0/24  for gstg prometheus servers
    # 10.250.8.0/24  for ops prometheus servers
    # 10.250.25.0/24 for ops thanos-rule serves
    # 10.232.3.0/24  for pre prometheus servers
    # 10.240.3.0/24  for testbed prometheus servers
    #
    # Port 9093 is for alertmanager API.
    "subnets" = ["10.251.17.0/24", "10.226.1.0/24", "10.250.8.0/24", "10.250.25.0/24", "10.232.3.0/24", "10.240.3.0/24"]
    "ports"   = ["9093"]
  }
}

####################################
# Default log filters for stackdriver
#####################################

variable "sd_log_filters" {
  type = map(string)

  default = {
    "exclude_logtypes" = "resource.type=\"gce_instance\" AND labels.tag!=\"haproxy\""
  }
}
