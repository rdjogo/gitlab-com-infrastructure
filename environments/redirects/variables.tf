variable "redirects" {
  type = map

  default = {
    "gitlab.org." = {
      "api.gitlab.org" = "http://doc.gitlab.com/ce/api/"
      "www.gitlab.org" = "https://about.gitlab.com/"
    }
    "gitlab.io." = {
      "www.gitlab.io" = "https://about.gitlab.com/"
    }
    "gitlab.com." = {
      "blog.gitlab.com"         = "https://about.gitlab.com/blog/"
      "arewefastyet.gitlab.com" = "https://gitlab.com/gitlab-org/gitlab-foss"
      "ce.gitlab.com"           = "https://gitlab.com/gitlab-org/gitlab-foss"
      "ee.gitlab.com"           = "https://gitlab.com/gitlab-org/gitlab"
      "hub.gitlab.com"          = "https://lab.github.com"
      "jobs.gitlab.com"         = "https://about.gitlab.com/jobs/"
    }
    "gitlap.com." = {
      "blue-moon.gitlap.com" = "https://about.gitlab.com/"
    }
    "remoteonly.org." = {
      "www.remoteonly.org"   = "https://about.gitlab.com/company/culture/all-remote/"
    }
  }
}

variable "apex_redirects" {
  type = map

  default = {
    "gitlab.org"     = "https://about.gitlab.com/"
    "gitlab.io"      = "https://about.gitlab.com/"
    "allremote.info" = "https://about.gitlab.com/company/culture/all-remote/"
    "remoteonly.org" = "https://about.gitlab.com/company/culture/all-remote/"
  }
}

# For now you have to manually create the TLS domains you want to support in
# Fastly and specify here the corresponding certificate host. If you don't need
# to support TLS you can ommit the entry and we'll use nonssl.global.fastly.net
# by default
variable "tls_domains" {
  type = map

  default = {
    "www.gitlab.org"       = "h3.shared.global.fastly.net"
    "www.gitlab.io"        = "h3.shared.global.fastly.net"
    "api.gitlab.org"       = "h3.shared.global.fastly.net"
    "blog.gitlab.com"      = "h3.shared.global.fastly.net"
    "blue-moon.gitlap.com" = "m2.shared.global.fastly.net"
    "www.remoteonly.org"   = "h3.shared.global.fastly.net"
  }
}

variable "tls_apex_domains_ips" {
  type = map

  default = {
    "gitlab.org" = [
      "151.101.2.49",
      "151.101.66.49",
      "151.101.194.49",
      "151.101.130.49"
    ]
    "gitlab.io" = [
      "151.101.2.49",
      "151.101.66.49",
      "151.101.194.49",
      "151.101.130.49"
    ]
    "allremote.info" = [
      "151.101.2.217",
      "151.101.66.217",
      "151.101.194.217",
      "151.101.130.217"
    ]
    "remoteonly.org" = [
      "151.101.2.217",
      "151.101.66.217",
      "151.101.194.217",
      "151.101.130.217"
    ]
  }
}

variable "cloudflare_account_id" {}
variable "cloudflare_api_key" {}
variable "cloudflare_email" {}
