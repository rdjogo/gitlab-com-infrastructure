variable "gitlab_org_zone_id" {}
variable "gitlab_io_zone_id" {}
variable "gitlab_com_zone_id" {}
variable "gitlap_com_zone_id" {}
variable "gitlab_net_zone_id" {}
variable "gitlab_review_app_zone_id" {}

variable "gitlap_com_a" {}
variable "gitlap_com_caa" {}
variable "gitlap_com_cname" {}
variable "gitlap_com_txt" {}
variable "gitlap_com_ns" {}
variable "gitlap_com_mx" {}
variable "gitlap_com_soa" {}

variable "gitlab_io_a" {}
variable "gitlab_io_caa" {}
variable "gitlab_io_txt" {}
variable "gitlab_io_ns" {}
variable "gitlab_io_mx" {}
variable "gitlab_io_soa" {}

variable "gitlab_com_a" {}
variable "gitlab_com_aaaa" {}
variable "gitlab_com_caa" {}
variable "gitlab_com_cname" {}
variable "gitlab_com_txt" {}
variable "gitlab_com_ns" {}
variable "gitlab_com_mx" {}
variable "gitlab_com_soa" {}

variable "staging_gitlab_com_a" {}
variable "staging_gitlab_com_aaaa" {}
variable "staging_gitlab_com_caa" {}
variable "staging_gitlab_com_cname" {}
variable "staging_gitlab_com_txt" {}
variable "staging_gitlab_com_ns" {}
variable "staging_gitlab_com_mx" {}
variable "staging_gitlab_com_soa" {}

variable "gitlab_org_a" {}
variable "gitlab_org_caa" {}
variable "gitlab_org_cname" {}
variable "gitlab_org_txt" {}
variable "gitlab_org_ns" {}
variable "gitlab_org_mx" {}
variable "gitlab_org_soa" {}

variable "gitlab_net_a" {}
variable "gitlab_net_caa" {}
variable "gitlab_net_cname" {}
variable "gitlab_net_txt" {}
variable "gitlab_net_ns" {}
variable "gitlab_net_mx" {}
variable "gitlab_net_soa" {}
variable "gitlab_net_alias" {}

variable "gitlab_review_app_a" {}
variable "gitlab_review_app_caa" {}
variable "gitlab_review_app_cname" {}
variable "gitlab_review_app_txt" {}
variable "gitlab_review_app_ns" {}
variable "gitlab_review_app_soa" {}

variable "cloudflare_api_key" {}
variable "cloudflare_account_id" {}
variable "cloudflare_email" {}
