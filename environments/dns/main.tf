## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/dns/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region = "us-east-1"
}

provider "cloudflare" {
  version    = "= 2.3"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

module "gitlab_com" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.com."
  a      = var.gitlab_com_a
  aaaa   = var.gitlab_com_aaaa
  caa    = var.gitlab_com_caa
  cname  = var.gitlab_com_cname
  txt    = var.gitlab_com_txt
  ns     = var.gitlab_com_ns
  mx     = var.gitlab_com_mx
  soa    = var.gitlab_com_soa
}

module "gitlab_net" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."
  a      = var.gitlab_net_a
  caa    = var.gitlab_net_caa
  cname  = var.gitlab_net_cname
  txt    = var.gitlab_net_txt
  ns     = var.gitlab_net_ns
  mx     = var.gitlab_net_mx
  soa    = var.gitlab_net_soa
}

module "gitlab_io" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.io."
  a      = var.gitlab_io_a
  caa    = var.gitlab_io_caa
  txt    = var.gitlab_io_txt
  ns     = var.gitlab_io_ns
  mx     = var.gitlab_io_mx
  soa    = var.gitlab_io_soa
}

module "gitlab_org" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.org."
  a      = var.gitlab_org_a
  caa    = var.gitlab_org_caa
  cname  = var.gitlab_org_cname
  txt    = var.gitlab_org_txt
  ns     = var.gitlab_org_ns
  mx     = var.gitlab_org_mx
  soa    = var.gitlab_org_soa
}

module "gitlab_review_app" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab-review.app."
  a      = var.gitlab_review_app_a
  caa    = var.gitlab_review_app_caa
  cname  = var.gitlab_review_app_cname
  txt    = var.gitlab_review_app_txt
  ns     = var.gitlab_review_app_ns
  soa    = var.gitlab_review_app_soa
}

module "gitlap_com" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlap.com."
  a      = var.gitlap_com_a
  caa    = var.gitlap_com_caa
  cname  = var.gitlap_com_cname
  txt    = var.gitlap_com_txt
  ns     = var.gitlap_com_ns
  mx     = var.gitlap_com_mx
  soa    = var.gitlap_com_soa
}

module "staging_gitlab_com" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "staging.gitlab.com."
  a      = var.staging_gitlab_com_a
  aaaa   = var.staging_gitlab_com_aaaa
  cname  = var.staging_gitlab_com_cname
  txt    = var.staging_gitlab_com_txt
  ns     = var.staging_gitlab_com_ns
  mx     = var.staging_gitlab_com_mx
  soa    = var.staging_gitlab_com_soa
}
