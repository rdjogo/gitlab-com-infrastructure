require 'json'
file = File.read(ARGV[0])
result_hash = JSON.parse(file)

STDERR.puts "Loading terraform plan..."

@resources = {}
sources = []

result_hash['resource_changes'].each do |resource|
  @resources[resource['address']] = resource
  sources << resource['address'] if resource['change']['actions'][0] == 'delete'
end

STDERR.puts "Loaded terraform plan..."

def validate(s, t)
  source = @resources[s]
  target = @resources[t]
  return false if source.nil? || target.nil?

  failure = 0

  begin
    raise "source is not deleted" unless source['change']['actions'][0] == 'delete'
    raise "target is not created" unless target['change']['actions'][0] == 'create'
    raise "zone_id does not match" unless source['change']['before']['zone_id'] == target['change']['after']['zone_id']
    if source['type'] == 'aws_route53_record'
      raise "target is not of type aws_route53_record" unless target['type'] == 'aws_route53_record'
      raise "name does not match" unless source['change']['before']['name'] == target['change']['after']['name']
      raise "records do not match" unless source['change']['before']['records'] == target['change']['after']['records']
      raise "ttl does not match" unless source['change']['before']['ttl'] == target['change']['after']['ttl']
      raise "type does not match" unless source['change']['before']['type'] == target['change']['after']['type']
    elsif source['type'] == 'cloudflare_record'
      raise "target is not of type cloudflare_record" unless target['type'] == 'cloudflare_record'
      raise "name does not match" unless source['change']['before']['name'] == target['change']['after']['name']
      raise "ttl does not match" unless source['change']['before']['ttl'] == target['change']['after']['ttl']
        raise "type does not match" unless source['change']['before']['type'] == target['change']['after']['type']
      if ["A", "CNAME", "TXT"].include?(source['change']['before']['type'])
        raise "value does not match" unless source['change']['before']['value'] == target['change']['after']['value']
        raise "proxied does not match" unless source['change']['before']['proxied'] == target['change']['after']['proxied']
      elsif source['change']['before']['type'] == "CAA"
        raise "data object does not match" unless source['change']['before']['data'] == target['change']['after']['data']
      elsif source['change']['before']['type'] == "MX"
        raise "priority does not match" unless source['change']['before']['priority'] == target['change']['after']['data']['priority'].to_i
        raise "value/target does not match" unless source['change']['before']['value'] == target['change']['after']['data']['target']
      else
        raise "target has unsupported record type"
      end
    elsif source['type'] == 'cloudflare_spectrum_application'
      raise "dns object does not match" unless source['change']['before']['dns'] == target['change']['after']['dns']
      raise "origin_direct does not match" unless source['change']['before']['origin_direct'] == target['change']['after']['origin_direct']
      raise "origin_dns does not match" unless source['change']['before']['origin_dns'] == target['change']['after']['origin_dns']
      raise "protocol does not match" unless source['change']['before']['protocol'] == target['change']['after']['protocol']
      raise "proxy_protocol does not match" unless source['change']['before']['proxy_protocol'] == target['change']['after']['proxy_protocol']
      raise "tls does not match" unless source['change']['before']['tls'] == target['change']['after']['tls']
      raise "traffic_type does not match" unless source['change']['before']['traffic_type'] == target['change']['after']['traffic_type']
    else
      raise "Unknown type #{source['type']}"
    end
  rescue StandardError => e
    STDERR.puts "Validation failed! #{s} => #{t} is invaild"
    STDERR.puts e.message
    STDERR.puts "source"
    STDERR.puts JSON.pretty_generate source['change']['before']
    STDERR.puts "target"
    STDERR.puts JSON.pretty_generate target['change']['after']
    return false
  end
return true
end

@substitutes = {
  # target pattern => source pattern
  'module.__DOMAIN__.module.a.aws_route53_record.a_aaaa["A_\1"]' => /aws_route53_record\.a\[\"([^"]+)\"\]/m,
  'module.__DOMAIN__.module.a.cloudflare_record.a_aaaa["A_\1_\2"]' => /cloudflare_record\.dns\["(.+)_A_([0-9]+)"\]/m,
  'module.__DOMAIN__.module.aaaa.aws_route53_record.a_aaaa["AAAA_\1"]' => /aws_route53_record\.aaaa\[\"([^"]+)\"\]/m,
  'module.__DOMAIN__.module.aaaa.cloudflare_record.a_aaaa["AAAA_\1_\2"]' => /cloudflare_record\.dns\["(.+)_AAAA_([0-9]+)"\]/m,
  'module.__DOMAIN__.module.caa.aws_route53_record.caa["\1"]' => /aws_route53_record\.caa\[\"([^"]+)\"\]/m,
  'module.__DOMAIN__.module.caa.cloudflare_record.caa["\1_\2"]' => /cloudflare_record\.dns\["(.+)_CAA_([0-9]+)"\]/m,
  'module.__DOMAIN__.module.cname.aws_route53_record.generic["CNAME_\1"]' => /aws_route53_record\.cname\[\"([^"]+)\"\]/m,
  'module.__DOMAIN__.module.cname.cloudflare_record.generic["CNAME_\1_\2"]' => /cloudflare_record\.dns\["(.+)_CNAME_([0-9]+)"\]/m,
  'module.__DOMAIN__.module.mx.aws_route53_record.mx["\1"]' => /aws_route53_record\.mx\[\"([^"]+)\"\]/m,
  'module.__DOMAIN__.module.mx.cloudflare_record.mx["\1_\2"]' => /cloudflare_record\.dns\["(.+)_MX_([0-9]+)"\]/m,
  'module.__DOMAIN__.module.ns.aws_route53_record.generic["NS_\1"]' => /aws_route53_record\.ns\[\"([^"]+)\"\]/m,
  'module.__DOMAIN__.module.soa.aws_route53_record.generic["SOA_\1"]' => /aws_route53_record\.soa\[\"([^"]+)\"\]/m,
  'module.__DOMAIN__.module.txt.aws_route53_record.generic["TXT_\1"]' => /aws_route53_record\.txt\[\"([^"]+)\"\]/m,
  'module.__DOMAIN__.module.txt.cloudflare_record.generic["TXT_\1_\2"]' => /cloudflare_record\.dns\["(.+)_TXT_([0-9]+)"\]/m,
  'module.__DOMAIN__.module.a.cloudflare_spectrum_application.a_aaaa["\1_tcp/\2"]' => /cloudflare_spectrum_application\.spectrum\["(.+)_tcp\/([0-9]+)"\]/m
}

@domains = [
  # substitutes for __DOMAIN__. Used to match above @substitutes against created resources.
  "gitlab_com",
  "gitlab_net",
  "gitlab_io",
  "gitlab_org",
  "gitlab_review_app",
  "gitlap_com",
  "staging_gitlab_com",
]

def process(str)
  result = ""
  @substitutes.each do |subst, re|
    result = str.gsub(re, subst)
    next if result == str
    @domains.each do |replacement|
      candidate = result.gsub(/__DOMAIN__/, replacement)

      # Because the regexes are fuzzy, we check, if the resource we want to move to, would actually be created.
      return candidate if @resources.has_key?(candidate)
    end
  end
  return false
end

count = 0

sources.each do |str|
  candidate = process(str)

  unless candidate
    STDERR.puts "#{str} NO MATCH"
    next
  end

break unless validate(str, candidate)
# STDERR.puts "move #{str} => #{candidate} is vaild"
 # break

  puts "terraform state mv '#{str}' '#{candidate}'"
  count +=1
end

STDERR.puts "Found #{count}/#{sources.length} substitutes"
