##################################
#
#  GitLab Shared Environment Configurations
#
##################################

# A CI user that can operate on the objects we describe here
resource "google_service_account" "ops-ci" {
  account_id   = "ops-ci"
  display_name = "ops-ci"
}

# A bucket to store all shared configurations
resource "google_storage_bucket" "gitlab-configs" {
  name = "gitlab-configs"

  versioning {
    enabled = "true"
  }

  storage_class = "MULTI_REGIONAL"

  labels = {
    tfmanaged = "yes"
  }
}

# All environments should have access to read the data stored here
resource "google_storage_bucket_iam_binding" "environments-RO" {
  bucket = "gitlab-configs"
  role   = "roles/storage.objectViewer"

  members = [
    "serviceAccount:k8s-workloads@gitlab-pre.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@gitlab-production.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@gitlab-staging-1.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@gitlab-ops.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@gitlab-org-ci-0d24e2.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-pre.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-production.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-staging-1.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-ops.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-org-ci-0d24e2.iam.gserviceaccount.com",
    "serviceAccount:terraform@gitlab-ops.iam.gserviceaccount.com",
    "serviceAccount:terraform@gitlab-pre.iam.gserviceaccount.com",
    "serviceAccount:terraform@gitlab-production.iam.gserviceaccount.com",
    "serviceAccount:terraform@gitlab-staging-1.iam.gserviceaccount.com",
    "serviceAccount:terraform@gitlab-testbed.iam.gserviceaccount.com",
  ]
}

# Only the ops instance is allowed to write data
resource "google_storage_bucket_iam_binding" "environments-RW" {
  bucket = "gitlab-configs"
  role   = "roles/storage.objectAdmin"

  members = [
    "serviceAccount:${google_service_account.ops-ci.email}",
  ]
}

# Create an encryption key and ring to encrypt/decrypt shared items
resource "google_kms_key_ring" "gitlab-shared-configs" {
  name     = "gitlab-shared-configs"
  location = "global"
}

# ops can encrypt and decrypt
resource "google_kms_key_ring_iam_binding" "gitlab-shared-configs-RW" {
  key_ring_id = "global/gitlab-shared-configs"
  role        = "roles/cloudkms.cryptoKeyEncrypterDecrypter"

  members = [
    "serviceAccount:${google_service_account.ops-ci.email}",
  ]
}

# all other environments can only decrypt
resource "google_kms_key_ring_iam_binding" "gitlab-shared-configs-RO" {
  key_ring_id = "global/gitlab-shared-configs"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:k8s-workloads@gitlab-ops.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@gitlab-pre.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@gitlab-production.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@gitlab-staging-1.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@gitlab-ops.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@gitlab-org-ci-0d24e2.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-ops.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-pre.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-production.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-staging-1.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-ops.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@gitlab-org-ci-0d24e2.iam.gserviceaccount.com",
    "serviceAccount:terraform@gitlab-ops.iam.gserviceaccount.com",
    "serviceAccount:terraform@gitlab-pre.iam.gserviceaccount.com",
    "serviceAccount:terraform@gitlab-production.iam.gserviceaccount.com",
    "serviceAccount:terraform@gitlab-staging-1.iam.gserviceaccount.com",
    "serviceAccount:terraform@gitlab-testbed.iam.gserviceaccount.com",
  ]
}

resource "google_kms_crypto_key" "config" {
  name            = "config"
  key_ring        = google_kms_key_ring.gitlab-shared-configs.self_link
  rotation_period = "7776000s"

  lifecycle {
    prevent_destroy = true
  }
}

