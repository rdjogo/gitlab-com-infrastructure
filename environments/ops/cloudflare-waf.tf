
# Cloudflare WAF Packages
#
# Runbook: https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/waf/cloudflare-terraform.md

locals {
  cloudflare_package_id = "1e334934fd7ae32ad705667f8c1057aa"
  owasp_package_id      = "c504870194831cd12c3fc0284f294abb"
}

resource "cloudflare_waf_group" "cloudflare-waf-group-off" {
  for_each = toset([
    "8df6c9aaddcbdf0eceb047bc50873697", #"Cloudflare Drupal"
    "cf6b50e84134de6c15b32edd3c03e201", #"Cloudflare Flash"
    "dc85d7a0289180d88640b056069dfa94", #"Cloudflare Joomla"
    "ea8687e59929c1fd05ba97574ad43f77", #"Cloudflare Magento"
    "c8485e461f935bead889fa0fe87dd76e", #"Cloudflare Php"
    "e54e546b9f9aa77eeafd7ec7b8a7f01e", #"Cloudflare Plone"
    "f90712123fb02287348dd34c0a282bb9", #"Cloudflare Specials"
    "99ae6db9c12ab9b1c0b85b9460ac6e64", #"Cloudflare Whmcs"
    "18ae91632f12c93ed523786a605936be", #"Cloudflare WordPress"
  ])
  group_id = each.key

  zone_id = var.cloudflare_zone_id
  mode    = "off"
}

resource "cloudflare_waf_group" "cloudflare-waf-group-on" {
  for_each = toset([
    "da9d75b083345c63f48e6fde5f617a8b", #"Cloudflare Miscellaneous"
  ])
  group_id = each.key

  zone_id = var.cloudflare_zone_id
  mode    = "on"
}

resource "cloudflare_waf_package" "owasp" {
  zone_id     = var.cloudflare_zone_id
  package_id  = local.owasp_package_id
  sensitivity = "low"
  action_mode = "challenge"
}

resource "cloudflare_waf_group" "owasp-waf-group-on" {
  for_each = toset([
    "07d700cf30fda7548be94ff01087d0c4", #"OWASP Bad Robots"
    "6293bbf04711b8511fc68dfd85d254b3", #"OWASP Common Exceptions"
    "5461c3844f6b5597986ae41d052a7cbd", #"OWASP Generic Attacks"
    "9ab3180a92fde4bfb370a6e0a33842d2", #"OWASP HTTP Policy"
    "89f0f131c955630147a5e8b6b13f5ca8", #"OWASP Protocol Anomalies"
    "bcdccfe389a27a990711d45d4bc67e24", #"OWASP Protocol Violations"
    "eac6910f74e3b80153bb52ac2fb82cc8", #"OWASP Request Limits"
    "9643591b65fc5d12056969519de4cb7b", #"OWASP Slr Et Lfi Attacks"
    "6035463b92082256c0c4cf5ce5028a98", #"OWASP Slr Et RFI Attacks"
    "40545574c88d71badb13d54240652ad6", #"OWASP Slr Et SQLi Attacks"
    "c013ec3948431586f65b3d2a1a15c037", #"OWASP Slr Et XSS Attacks"
    "a0608e0c654339c108ca9715a7b1fb2c", #"OWASP SQL Injection Attacks"
    "3d8fb0c18b5a6ba7682c80e94c7937b2", #"OWASP Tight Security"
    "5664cc2f83dd55b71929c062921078aa", #"OWASP Trojans"
    "a7b84f7c2c7fc5ef46ff02095e6daaec", #"OWASP Uri SQL Injection Attacks"
    "cfda825dfda411ea218cb70e6c88e82e", #"OWASP Uri XSS Attacks"
    "d508327aee37c147e03873f79288bb1d", #"OWASP XSS Attacks"
  ])
  group_id = each.key

  zone_id = var.cloudflare_zone_id
  mode    = "on"
}

resource "cloudflare_waf_group" "owasp-waf-group-off" {
  for_each = toset([
    "add7f2d8bb23532a8a605ff84af03219", #"OWASP Slr Et Joomla Attacks"
    "87fdf1fd5d6be43439ad0ef465455f92", #"OWASP Slr Et PhpBB Attacks"
    "6dc214c2b087e20f6952dccf257b0384", #"OWASP Slr Et WordPress Attacks"
  ])
  group_id = each.key

  zone_id = var.cloudflare_zone_id
  mode    = "on"
}
