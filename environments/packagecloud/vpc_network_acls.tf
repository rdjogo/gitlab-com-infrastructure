###############
# Network ACLs
###############

resource "aws_network_acl" "packagecloud" {
  vpc_id = aws_vpc.packagecloud.id
  subnet_ids = [
    aws_subnet.public_subnet.id
  ]

  tags = {
    Name        = var.tag_name
    Terraform   = "true"
    Environment = var.environment
  }
}

locals {
  public_inbound_acl_rules = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = var.all_ips_cidr
    },
    {
      rule_number     = 101
      rule_action     = "allow"
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      ipv6_cidr_block = var.all_ipv6_ips_cidr
    }
  ]

  public_outbound_acl_rules = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = var.all_ips_cidr
    },
    {
      rule_number     = 101
      rule_action     = "allow"
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      ipv6_cidr_block = var.all_ipv6_ips_cidr
    }
  ]
}

resource "aws_network_acl_rule" "public_inbound" {
  network_acl_id  = aws_network_acl.packagecloud.id
  count           = length(local.public_inbound_acl_rules)
  egress          = false
  rule_number     = local.public_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = local.public_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(local.public_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(local.public_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(local.public_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(local.public_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = local.public_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(local.public_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(local.public_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "public_outbound" {
  network_acl_id  = aws_network_acl.packagecloud.id
  count           = length(local.public_outbound_acl_rules)
  egress          = true
  rule_number     = local.public_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = local.public_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(local.public_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(local.public_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(local.public_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(local.public_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = local.public_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(local.public_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(local.public_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}
