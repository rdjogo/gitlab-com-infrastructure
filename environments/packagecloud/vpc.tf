#######
# VPC
#######

resource "aws_vpc" "packagecloud" {
  cidr_block                       = var.vpc_cidr_block
  assign_generated_ipv6_cidr_block = true
  enable_dns_hostnames             = true
  enable_dns_support               = true
  instance_tenancy                 = "default"
  tags = {
    Name        = var.tag_name
    Terraform   = "true"
    Environment = var.environment
  }
}

##########
# Subnets
##########

resource "aws_subnet" "public_subnet" {
  vpc_id            = aws_vpc.packagecloud.id
  cidr_block        = "10.2.0.0/24" # 256 IPs
  ipv6_cidr_block   = cidrsubnet(aws_vpc.packagecloud.ipv6_cidr_block, 8, 0)
  availability_zone = var.zone

  tags = {
    Name        = var.tag_name
    Terraform   = "true"
    Environment = var.environment
    Public      = "true"
  }

  depends_on = [aws_vpc.packagecloud]
}

###############
# DHCP options
###############

resource "aws_vpc_dhcp_options" "aws_dns" {
  domain_name_servers = ["AmazonProvidedDNS"]

  tags = {
    Name        = var.tag_name
    Terraform   = "true"
    Environment = var.environment
  }
}

resource "aws_vpc_dhcp_options_association" "packagecloud_vpc" {
  vpc_id          = aws_vpc.packagecloud.id
  dhcp_options_id = aws_vpc_dhcp_options.aws_dns.id
}

###################
# Internet Gateway
###################

resource "aws_internet_gateway" "packagecloud" {
  vpc_id = aws_vpc.packagecloud.id

  tags = {
    Name        = var.tag_name
    Terraform   = "true"
    Environment = var.environment
  }

  depends_on = [aws_vpc.packagecloud]
}
