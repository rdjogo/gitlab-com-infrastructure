## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/packagecloud/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-west-1"
  version = "2.45.0"
}

#######
# Keys
#######

# remove when chef is installed
resource "aws_key_pair" "cpallares" {
  key_name   = "cpallares"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDGm9oOInXiUVcat1cDtSsdVB6Ds0mL/gzigXPS+OHfSljZqU7J0z6BaGw3enJyhk0ZRzXF4ehJaG2ImIOBj5OSx7tt1ZIZWYNO7HXzMAzBNrtq1LOBJH+kQ5XDawbJ9mOjAPRARQI6HiEpXK+Mf1/VFYvhYpXynswUhf1RButa+Yir16nYhT1cJ5IiMItyiafdZSrMZtqW/BdHJj9otIMqqXdGL4HHGQzBMJbaKLDkzrPvtxO7If6do6eIJiJa9i8HNX/AjKb8y+jZeVDpxiuGCG8GbpbxHvWirJtx4i+oxPxmgNmw7+YzrLrsGqX8kQ3RcjEEwtffjwj023ClCl39 cindy@gitlab.com"

}

########################
# Packagecloud Instance
########################

resource "aws_instance" "packagecloud" {
  ami                         = data.aws_ami.ubuntu-xenial.id
  associate_public_ip_address = true
  availability_zone           = var.zone
  cpu_core_count              = 8
  cpu_threads_per_core        = 2
  disable_api_termination     = true
  ebs_optimized               = true
  get_password_data           = false
  instance_type               = "c4.4xlarge"
  depends_on                  = ["aws_internet_gateway.packagecloud"]
  key_name                    = aws_key_pair.cpallares.key_name
  monitoring                  = false
  ipv6_addresses              = ["2600:1f1c:2d4:8900:17a1:2e94:9f90:f91e"]
  security_groups             = []
  source_dest_check           = true
  subnet_id                   = aws_subnet.public_subnet.id
  tags = {
    Name        = var.packages_tag_name
    Terraform   = "true"
    Environment = var.environment
  }
  tenancy = "default"
  volume_tags = {
    Name        = "boot-disk-${var.packages_tag_name}"
    Terraform   = "true"
    Environment = var.environment
  }
  vpc_security_group_ids = [
    aws_security_group.packagecloud.id,
    aws_security_group.nessus_scan_target_group.id
  ]

  root_block_device {
    volume_type           = "gp2"
    volume_size           = 1024
    delete_on_termination = false
  }

}

########################
# Volumes
########################

resource "aws_ebs_volume" "packagecloud_data" {
  availability_zone = var.zone
  encrypted         = true
  size              = 3072
  tags = {
    "Name"      = "data-${var.packages_tag_name}"
    Terraform   = "true"
    Environment = var.environment
  }
  type = "gp2"
}

resource "aws_volume_attachment" "packagecloud_data" {
  device_name = "/dev/xvdb"
  volume_id   = aws_ebs_volume.packagecloud_data.id
  instance_id = aws_instance.packagecloud.id
}

#######################################################
#
# Tenable.IO local Nessus scanner
#
#######################################################

module "nessus_scanner" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/aws-nessus-scanner?ref=v1.0.0"

  scanner_name        = "packagecloud-nessus-scanner"
  tenable_linking_key = "064e9fabdbe057bce74bcef638c87a8bffe296b1896251a03b4670342f10fd33"
  vpc_id              = aws_vpc.packagecloud.id
  subnet_id           = aws_subnet.public_subnet.id
  instance_type       = "t3.xlarge"
  instance_name       = "packagecloud-nessus-scanner"

  instance_tags = {
    Instance = "packagecloud"
    Role     = "security-scanner"
    Projects = "tenable"
  }
}

resource "aws_security_group" "nessus_scan_target_group" {
  name        = "Scan target security group"
  description = "Allowing inbound scanning from nessus-scanner"
  vpc_id      = aws_vpc.packagecloud.id
  tags = {
    role        = "security-scanner-access"
    environment = "packagecloud"
  }
}

resource "aws_security_group_rule" "allow_nessus_access" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.nessus_scan_target_group.id
  source_security_group_id = module.nessus_scanner.security_group_id
}
