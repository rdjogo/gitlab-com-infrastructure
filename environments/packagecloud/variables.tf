variable "zone" {
  description = "Availability zone"
  type        = string
  default     = "us-west-1c"
}

variable "environment" {
  description = "Environment name"
  type        = string
  default     = "packagecloud"
}

variable "all_ips_cidr" {
  description = "Wildcard address"
  type        = string
  default     = "0.0.0.0/0"
}

variable "all_ipv6_ips_cidr" {
  description = "IPv6 wildcard address"
  type        = string
  default     = "::/0"
}

variable "packages_tag_name" {
  description = "Tag name for instance"
  type        = string
  default     = "packages.gitlab.com"
}

variable "tag_name" {
  description = "Tag name for subnet"
  type        = string
  default     = "packagecloud"
}

variable "vpc_cidr_block" {
  description = "VPC cidr block"
  type        = string
  default     = "10.2.0.0/22" # 1,024 IPs
}
