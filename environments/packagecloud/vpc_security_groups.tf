##################
# Security Groups
##################

resource "aws_security_group" "packagecloud" {
  description            = "Packagecloud security group"
  name                   = "packages-gitlab-com"
  revoke_rules_on_delete = false
  tags = {
    Name        = var.tag_name
    Terraform   = "true"
    Environment = var.environment
  }
  vpc_id = aws_vpc.packagecloud.id

  depends_on = [aws_vpc.packagecloud]
}

########################
# Security Groups Rules
########################

resource "aws_security_group_rule" "ingress-port-22" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.packagecloud.id
  cidr_blocks = [
    var.all_ips_cidr,
  ]
}

resource "aws_security_group_rule" "ingress-port-80" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.packagecloud.id
  cidr_blocks = [
    var.all_ips_cidr,
  ]
}

resource "aws_security_group_rule" "ingress-port-443" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.packagecloud.id
  cidr_blocks = [
    var.all_ips_cidr,
  ]
}

resource "aws_security_group_rule" "ingress-icmp" {
  type              = "ingress"
  from_port         = 8
  to_port           = 8
  protocol          = "icmp"
  security_group_id = aws_security_group.packagecloud.id
  cidr_blocks = [
    var.all_ips_cidr,
  ]
}

resource "aws_security_group_rule" "ingress-prometheus" {
  type              = "ingress"
  from_port         = 9100
  to_port           = 9100
  protocol          = "tcp"
  security_group_id = aws_security_group.packagecloud.id
  cidr_blocks = [
    "13.68.87.12/32",    # prometheus.gitlab.com
    "35.185.16.254/32",  # prometheus-01-inf-gprd
    "34.74.136.38/32",   # prometheus-02-inf-gprd
    "35.227.109.92/32",  # gitlab-ops / prometheus-01-inf-ops
    "35.237.131.211/32", # gitlab-ops /prometheus-02-inf-ops
  ]
}

resource "aws_security_group_rule" "egress-allow-all-ipv4" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.packagecloud.id
  cidr_blocks = [
    var.all_ips_cidr,
  ]
}

resource "aws_security_group_rule" "ingress-port-22-ipv6" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.packagecloud.id
  ipv6_cidr_blocks = [
    var.all_ipv6_ips_cidr
  ]
}

resource "aws_security_group_rule" "ingress-port-80-ipv6" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.packagecloud.id
  ipv6_cidr_blocks = [
    var.all_ipv6_ips_cidr
  ]
}

resource "aws_security_group_rule" "ingress-port-443-ipv6" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.packagecloud.id
  ipv6_cidr_blocks = [
    var.all_ipv6_ips_cidr
  ]
}

resource "aws_security_group_rule" "ingress-icmp-ipv6" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "icmpv6"
  security_group_id = aws_security_group.packagecloud.id
  ipv6_cidr_blocks = [
    var.all_ipv6_ips_cidr
  ]
}

resource "aws_security_group_rule" "egress-allow-all-ipv6" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.packagecloud.id
  ipv6_cidr_blocks = [
    var.all_ipv6_ips_cidr
  ]
}
