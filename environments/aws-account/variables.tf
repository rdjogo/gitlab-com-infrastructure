variable "nessus_tags" {
  type = "map"

  default = {
    Name        = "nessus-scanner"
    Terraform   = "true"
    Environment = "aws-account"
  }

}
