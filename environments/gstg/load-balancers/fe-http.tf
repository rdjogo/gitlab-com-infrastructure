resource "google_compute_forwarding_rule" "fe_forwarding_rule_http" {
  name                  = "gitlab-frontend-http"
  project               = var.project
  region                = var.region
  target                = google_compute_target_pool.fe_lb_http_pool.self_link
  load_balancing_scheme = "EXTERNAL"
  port_range            = "80"
  ip_address            = google_compute_address.fe_external_ip.address
}

resource "google_compute_target_pool" "fe_lb_http_pool" {
  project          = var.project
  name             = "gitlab-frontend-http-pool"
  region           = var.region
  session_affinity = "NONE"
  instances        = [var.fe_http_instances]

  health_checks = [
    google_compute_http_health_check.fe_lb_http_health_check.self_link,
  ]
}

resource "google_compute_http_health_check" "fe_lb_http_health_check" {
  project             = var.project
  name                = "fe-lb-http-health-check"
  host                = "gitlab.com"
  port                = "80"
  request_path        = "/help"
  timeout_sec         = 2
  check_interval_sec  = 2
  healthy_threshold   = 2
  unhealthy_threshold = 2
}
