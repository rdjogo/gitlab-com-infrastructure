resource "google_compute_forwarding_rule" "fe_forwarding_rule_https" {
  name                  = "gitlab-frontend-https"
  project               = var.project
  region                = var.region
  target                = google_compute_target_pool.fe_lb_https_pool.self_link
  load_balancing_scheme = "EXTERNAL"
  port_range            = "443"
  ip_address            = google_compute_address.fe_external_ip.address
}

resource "google_compute_target_pool" "fe_lb_https_pool" {
  project          = var.project
  name             = "gitlab-frontend-https-pool"
  region           = var.region
  session_affinity = "NONE"
  instances        = [var.fe_https_instances]

  health_checks = [
    google_compute_http_health_check.fe_lb_http_health_check.self_link,
  ]
}
