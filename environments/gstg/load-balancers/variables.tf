variable "fe_http_instances" {
  type        = "list"
  description = "The list of instances to add to the http backend pool"
}

variable "fe_https_instances" {
  type        = "list"
  description = "The list of instances to add to the https backend pool"
}

variable "fe_ssh_instances" {
  type        = "list"
  description = "The list of instances to add to the ssh backend pool"
}

variable "region" {
  type        = string
  description = "The target region"
}

variable "project" {
  type        = string
  description = "The target project"
}

# variable "subnetwork" {
#   type        = string
#   description = "The target subnetwork"
# }

