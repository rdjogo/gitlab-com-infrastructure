variable "oauth2_client_id_monitoring" {}

variable "oauth2_client_secret_monitoring" {}

variable "gitlab_net_zone_id" {}

variable "gitlab_com_zone_id" {}

variable "gitlab_io_zone_id" {}

variable "gitlab_static_net_zone_id" {}

variable "static_objects_cache_elasticsearch_auth" {}

variable "static_objects_cache_sentry_key" {}

variable "static_objects_external_storage_token" {}

variable "bootstrap_script_version" {
  default = 8
}

#############################
# Default firewall
# rule for allowing
# all protocols on all
# ports
#
# 10.224.x.x: all of gstg
# 10.250.7.x: ops runner
# 10.250.8.11/32: nessus scanner
# 10.250.10.x: chatops runner
# 10.250.12.x: release runner
# 10.12.0.0/14: pod address range in gitlab-ops for runners
###########################

variable "internal_subnets" {
  type    = list(string)
  default = ["10.224.0.0/13", "10.250.7.0/24", "10.250.8.11", "10.250.10.0/24", "10.250.12.0/24", "10.12.0.0/14"]
}

variable "other_monitoring_subnets" {
  type = list(string)

  # 10.219.1.0/24: gprd
  # 10.251.17.0/24: dr
  default = ["10.219.1.0/24", "10.251.17.0/24"]
}

##################
# Network Peering
##################

variable "network_env" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-staging-1/global/networks/gstg"
}

variable "peer_networks" {
  type = map(list(string))

  default = {
    "names" = ["ops", "gprd", "vault-nonprod"]
    "links" = [
      "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops",
      "https://www.googleapis.com/compute/v1/projects/gitlab-production/global/networks/gprd",
      "https://www.googleapis.com/compute/v1/projects/gitlab-vault-nonprod/global/networks/vault",
    ]
  }
}

######################

variable "base_chef_run_list" {
  default = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
}

variable "empty_chef_run_list" {
  default = "\"\""
}

variable "dns_zone_name" {
  default = "gitlab.com"
}

variable "monitoring_hosts" {
  type = map(list(string))

  default = {
    "names" = ["alerts", "prometheus", "prometheus-app", "prometheus-db", "prometheus-test"]
    "ports" = [9093, 9090, 9090, 9090, 9090]
  }
}

#### GCP load balancing

# The top level domain record for the GitLab deployment.
# For production this should be set to "gitlab.com"

variable "lb_fqdns" {
  type    = list(string)
  default = ["canary.staging.gitlab.com"]
}

#####

variable "lb_fqdns_altssh" {
  type    = list(string)
  default = ["altssh.gstg.gitlab.com"]
}

variable "lb_fqdns_registry" {
  type    = list(string)
  default = ["registry.staging.gitlab.com"]
}

variable "lb_fqdns_cny" {
  type    = list(string)
  default = []
}

variable "lb_fqdns_pages" {
  type    = list(string)
  default = ["*.pages.gstg.gitlab.io"]
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.gstg.gitlab.com"]
}

variable "lb_fqdns_internal" {
  type    = list(string)
  default = ["int.gstg.gitlab.net"]
}

variable "lb_fqdns_tmp_internal" {
  type    = list(string)
  default = ["int-tmp.gstg.gitlab.net"]
}

variable "lb_fqdns_internal_praefect" {
  type    = list(string)
  default = []
}

variable "lb_fqdns_internal_pgbouncer" {
  type    = list(string)
  default = ["pgbouncer.int.gstg.gitlab.net"]
}

variable "lb_fqdns_internal_pgbouncer_sidekiq" {
  type    = list(string)
  default = ["pgbouncer-sidekiq.int.gstg.gitlab.net"]
}

variable "lb_fqdns_internal_patroni" {
  type    = list(string)
  default = ["patroni.int.gstg.gitlab.net"]
}

#
# For every name there must be a corresponding
# forwarding port range and health check port
#

variable "tcp_lbs" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https", "ssh"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "tcp_lbs_internal" {
  type = map(list(string))

  default = {
    "names"                  = ["http-internal", "https-internal", "alt-https-internal", "ssh-internal"]
    "forwarding_port_ranges" = ["80", "443", "11443", "22"]
    "health_check_ports"     = ["8001", "8002", "8002", "8003"]
  }
}

variable "tcp_lbs_internal_praefect" {
  type = map(list(string))

  default = {
    "names"                      = ["praefect"]
    "forwarding_port_ranges"     = ["2305"]
    "health_check_ports"         = ["9652"]
    "health_check_request_paths" = ["/metrics"]
  }
}

variable "tcp_lbs_pages" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_altssh" {
  type = map(list(string))

  default = {
    "names"                      = ["https"]
    "forwarding_port_ranges"     = ["443"]
    "health_check_ports"         = ["8003"]
    "health_check_request_paths" = ["/-/available-ssh"]
  }
}

variable "tcp_lbs_registry" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_cny" {
  type = map(list(string))

  default = {
    "names"                  = []
    "forwarding_port_ranges" = []
    "health_check_ports"     = []
  }
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

#######################

variable "public_ports" {
  type = map(list(string))

  default = {
    "api"           = []
    "bastion"       = [22]
    "blackbox"      = []
    "console"       = []
    "consul"        = []
    "deploy"        = []
    "runner"        = []
    "db-dr"         = []
    "pgbouncer"     = []
    "fe-lb"         = [22, 80, 443]
    "geo-secondary" = [22]
    "git"           = []
    "patroni"       = []
    "pubsubbeat"    = []
    "redis"         = []
    "redis-sidekiq" = []
    "redis-cache"   = []
    "sidekiq"       = []
    "sd-exporter"   = []
    "stor"          = []
    "thanos"        = []
    "web"           = []
    "web-pages"     = []
    "monitoring"    = []
    "praefect"      = []
  }
}

variable "environment" {
  default = "gstg"
}

variable "format_data_disk" {
  default = "true"
}

variable "project" {
  default = "gitlab-staging-1"
}

variable "region" {
  default = "us-east1"
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-gstg-chef-bootstrap"
    bootstrap_key     = "gitlab-gstg-bootstrap-validation"
    bootstrap_keyring = "gitlab-gstg-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "data_disk_sizes" {
  type = map(string)

  default = {
    "file"       = "2000"
    "file-hdd"   = "2000"
    "share"      = "1500"
    "pages"      = "4000"
    "patroni"    = "1500"
    "prometheus" = "50"
  }
}

variable "machine_types" {
  type = map(string)

  default = {
    "alerts"                           = "n1-standard-1"
    "api"                              = "n1-standard-16"
    "bastion"                          = "g1-small"
    "blackbox"                         = "n1-standard-1"
    "camoproxy"                        = "n1-standard-1"
    "console"                          = "n1-standard-4"
    "console-migrate-shard"            = "n1-standard-4"
    "consul"                           = "n1-standard-4"
    "deploy"                           = "n1-standard-2"
    "runner"                           = "n1-standard-2"
    "db-dr"                            = "n1-standard-8"
    "fe-lb"                            = "n1-standard-4"
    "git"                              = "n1-standard-16"
    "geo-secondary"                    = "n1-standard-16"
    "gitlab-gke"                       = "n1-standard-4"
    "gitlab-gke-low-urgency-cpu-bound" = "c2-standard-4"
    "gitlab-gke-urgent-other"          = "n1-standard-4"
    "gitlab-gke-urgent-cpu-bound"      = "c2-standard-4"
    "influxdb"                         = "n1-standard-4"
    "pgbouncer"                        = "n1-standard-4"
    "monitoring"                       = "n1-standard-4"
    "patroni"                          = "n1-standard-8"
    "patroni-migrate"                  = "n1-highmem-96"
    "praefect"                         = "n1-standard-2"
    "praefect-db"                      = "db-custom-1-3840"
    "redis"                            = "c2-standard-8"
    "redis-sidekiq"                    = "c2-standard-4"
    "redis-cache"                      = "c2-standard-4"
    "redis-cache-sentinel"             = "n1-standard-1"
    "sd-exporter"                      = "n1-standard-1"
    # Important note: when changing the number of cores on sidekiq workers,
    # please be sure to reconfigure the respective sidekiqProcessCount in
    # https://ops.gitlab.net/gitlab-cookbooks/chef-repo/blob/master/tools/sidekiq-config/sidekiq-queue-configurations.libsonnet
    "sidekiq-elasticsearch"         = "n1-standard-2"
    "sidekiq-low-urgency-cpu-bound" = "n1-standard-2"
    "sidekiq-catchall"              = "n1-standard-8"
    "sidekiq-urgent-other"          = "n1-standard-4"
    "sidekiq-urgent-cpu-bound"      = "c2-standard-4"
    "stor"                          = "n1-standard-32"
    "stor-hdd"                      = "n1-standard-8"
    "thanos-compact"                = "n1-highmem-2"
    "thanos-store"                  = "n1-highmem-2"
    "web"                           = "n1-standard-16"
    "web-pages"                     = "n1-standard-4"
    "stor-pages"                    = "n1-standard-4"
    "stor-share"                    = "n1-standard-4"
  }
  # We currently have different instance types
  # for pages and share in gprd so these are
  # also needed for gstg.
}

variable "node_count" {
  type = map(string)

  default = {
    "api"                           = 3
    "bastion"                       = 2
    "blackbox"                      = 1
    "camoproxy"                     = 2
    "console"                       = 1
    "deploy"                        = 1
    "deploy-cny"                    = 0
    "runner"                        = 1
    "consul"                        = 5
    "db-dr"                         = 2
    "fe-lb"                         = 3
    "fe-lb-pages"                   = 2
    "fe-lb-altssh"                  = 2
    "fe-lb-registry"                = 2
    "fe-lb-cny"                     = 0
    "git"                           = 3
    "geo-secondary"                 = 1
    "pages"                         = 1
    "patroni"                       = 6
    "patroni-migrate"               = 3
    "pgbouncer"                     = 3
    "pgbouncer-sidekiq"             = 3
    "pglogical"                     = 1
    "redis"                         = 3
    "redis-sidekiq"                 = 3
    "redis-cache"                   = 3
    "redis-cache-sentinel"          = 3
    "sd-exporter"                   = 1
    "share"                         = 1
    "sidekiq-elasticsearch"         = 0
    "sidekiq-low-urgency-cpu-bound" = 0
    "sidekiq-catchall"              = 1
    "sidekiq-urgent-other"          = 0
    "sidekiq-urgent-cpu-bound"      = 0
    "stor"                          = 4
    "stor-hdd"                      = 1
    "stor-zfs"                      = 1
    "thanos-compact"                = 1
    "thanos-store"                  = 1
    "multizone-stor"                = 0
    "web"                           = 3
    "web-pages"                     = 2
    "web-cny"                       = 0
    "api-cny"                       = 0
    "git-cny"                       = 0
    "prometheus"                    = 2
    "prometheus-app"                = 2
    "prometheus-db"                 = 2
    "prometheus-test"               = 1
    "alerts"                        = 0
    "praefect"                      = 3
  }
}

variable "subnetworks" {
  type = map(string)

  default = {
    "api"                     = "10.224.12.0/24"
    "bastion"                 = "10.224.20.0/24"
    "camoproxy"               = "10.224.35.0/24"
    "console"                 = "10.224.21.0/24"
    "console-migrate-shard"   = "10.224.31.0/29"
    "consul"                  = "10.224.4.0/24"
    "db-dr-delayed"           = "10.224.24.0/24"
    "db-dr-archive"           = "10.224.25.0/24"
    "deploy"                  = "10.224.15.0/24"
    "deploy-cny"              = "10.224.17.0/24"
    "fe-lb"                   = "10.224.14.0/24"
    "fe-lb-altssh"            = "10.224.19.0/24"
    "fe-lb-pages"             = "10.224.18.0/24"
    "fe-lb-registry"          = "10.224.23.0/24"
    "fe-lb-cny"               = "10.224.27.0/24"
    "git"                     = "10.224.13.0/24"
    "geo-secondary"           = "10.224.39.0/24"
    "monitoring"              = "10.226.1.0/24"
    "patroni"                 = "10.224.29.0/24"
    "patroni-migrate"         = "10.224.42.0/24"
    "pgbouncer"               = "10.224.9.0/24"
    "pgbouncer-sidekiq"       = "10.224.10.0/24"
    "pglogical"               = "10.224.30.0/24"
    "pubsubbeat"              = "10.226.2.0/24"
    "redis"                   = "10.224.7.0/24"
    "redis-cache"             = "10.224.8.0/24"
    "redis-sidekiq"           = "10.224.22.0/24"
    "runner"                  = "10.224.16.0/24"
    "sidekiq"                 = "10.225.1.0/24"
    "stor"                    = "10.224.2.0/23"
    "stor-hdd"                = "10.224.43.0/24"
    "stor-zfs"                = "10.224.36.0/24"
    "thanos-compact"          = "10.226.5.0/24"
    "thanos-store"            = "10.226.4.0/24"
    "web"                     = "10.224.1.0/24"
    "web-pages"               = "10.224.26.0/24"
    "pages"                   = "10.224.32.0/24"
    "share"                   = "10.224.33.0/24"
    "gitlab-gke"              = "10.224.34.0/24"
    "gitlab-gke-pod-cidr"     = "10.227.0.0/16"
    "gitlab-gke-service-cidr" = "10.228.0.0/16"
    "praefect"                = "10.224.37.0/24"
  }
}

variable "service_account_email" {
  type = string

  default = "terraform@gitlab-staging-1.iam.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-staging-1.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_service_account" {
  type    = string
  default = "postgres-wal-archive@gitlab-staging-1.iam.gserviceaccount.com"
}

# Service account used to do automated backup testing
# in https://gitlab.com/gitlab-restore/postgres-gprd
variable "gcs_postgres_restore_service_account" {
  type    = string
  default = "postgres-automated-backup-test@gitlab-restore.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_kms_key_id" {
  type    = string
  default = "projects/gitlab-staging-1/locations/global/keyRings/gitlab-secrets/cryptoKeys/gstg-postgres-wal-archive"
}

variable "postgres_backup_retention_days" {
  type    = string
  default = "5"
}

variable "egress_ports" {
  type    = list(string)
  default = ["80", "443"]
}

variable "web_egress_ports" {
  type    = list(string)
  default = ["80", "443", "9243"]
}

# TODO: This is a temporary variable as we're still rolling
# the egress rules to staging first and we don't want it in production yet.
# It should be removed in favor of appending port 22 to `egress_ports` in main.tf directly.
variable "deploy_egress_ports" {
  type    = list(string)
  default = ["80", "443", "22"]
}

variable "console_egress_ports" {
  type    = list(string)
  default = ["80", "443", "9243", "22"]
}

# Gitaly does git pulls, and thus needs SSH as well.  Ideally this could be targeted
# e.g. to staging.gitlab.com only, but this will do for now
variable "gitaly_egress_ports" {
  type    = list(string)
  default = ["80", "443", "22"]
}

variable "os_boot_image" {
  type = map(string)

  default = {
    "camoproxy" = "ubuntu-os-cloud/ubuntu-1804-lts"
    "fe-lb"     = "ubuntu-os-cloud/ubuntu-1804-lts"
    "fe-lb-ci"  = "ubuntu-os-cloud/ubuntu-1804-lts"
  }
}

####################################
# Camo proxy values
#####################################

variable "camoproxy_domain" {
  type    = string
  default = "gitlab-static.net" # Supplemented by camoproxy_domain_prefix
}

variable "camoproxy_domain_prefix" {
  type    = string
  default = "staging."
}

variable "camoproxy_serviceport" {
  type    = string
  default = 80 # Actually haproxy, or whatever is in front of camoproxy doing blacklisting
}

variable "camoproxy_hostname" {
  type    = string
  default = "user-content"
}

# This is permanent, regardless of any other egress port changes.
variable "camoproxy_egress_ports" {
  type    = list(string)
  default = ["80", "443"]
}

#######################
# pubsubbeat config
#######################

variable "pubsubbeats" {
  type = map(list(string))

  default = {
    "names" = [
      "gitaly",
      "haproxy",
      "pages",
      "postgres",
      "production",
      "system",
      "workhorse",
      "rspec",
      "sidekiq",
      "api",
      "nginx",
      "gitlab-shell",
      "shell",
      "rails",
      "unstructured",
      "unicorn",
      "application",
      "registry",
      "redis",
      "consul",
      "runner",
      "gke",
      "camoproxy",
      "monitoring",
      "praefect",
      "puma",
    ]
    "machine_types" = [
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-2",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
    ]
  }
}

### Object Storage Configuration

variable "versioning" {
  type    = string
  default = "true"
}

variable "artifact_age" {
  type    = string
  default = "30"
}

variable "upload_age" {
  type    = string
  default = "30"
}

variable "lfs_object_age" {
  type    = string
  default = "30"
}

variable "package_repo_age" {
  type    = string
  default = "30"
}

variable "storage_class" {
  type    = string
  default = "MULTI_REGIONAL"
}

variable "storage_log_age" {
  type    = string
  default = "7"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

#################
# Monitoring whitelist
#################

#################
# Allow traffic from the ops
# network from grafana
#################

variable "monitoring_whitelist_prometheus" {
  type = map(list(string))

  default = {
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    # 10.250.8.x for the ops prometheus servers
    #
    # Port 9090 is for prometheus
    # Ports 10900-10902 is for thanos
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]
    "ports"   = ["9090", "10900", "10901", "10902"]
  }
}

variable "monitoring_whitelist_thanos" {
  type = map(list(string))

  default = {
    # 10.250.8.x for the ops prometheus servers
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]
    "ports"   = ["10901", "10902"]
  }
}

#################
# Allow traffic from the ops
# network from the alerts manager
#################
variable "monitoring_whitelist_alerts" {
  type = map(list(string))

  default = {
    # 10.250.8.x for the ops alerts servers
    "subnets" = ["10.250.8.0/24"]
    "ports"   = ["9093"]
  }
}

####################################
# Default log filters for stackdriver
#####################################

variable "sd_log_filters" {
  type = map(string)

  default = {
    "exclude_logtypes" = "resource.type=\"gce_instance\" AND labels.tag!=\"haproxy\""
  }
}

variable "master_cidr_subnets" {
  type = map(string)

  default = {
    "gitlab-gke" = "172.16.0.0/28"
  }
}

variable "cloudflare_zone_name" {}

variable "cloudflare_zone_id" {}

variable "cloudflare_api_key" {}

variable "cloudflare_account_id" {}

variable "cloudflare_email" {}
