// Configure the provider using the terraform admin project
// Reference: https://cloud.google.com/community/tutorials/managing-gcp-projects-with-terraform
provider "google" {
  project = "env-zero"
  region  = "us-east1"
  version = "~> 2.20.0"
}

// Use Terraform Remote State backed by Google Cloud Storage
terraform {
  backend "gcs" {
    bucket = "gitlab-com-infrastructure"
    prefix = "env-projects/terraform-states"
  }
}

// Create the projects
module "gitlab-ci" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.2.2"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = "155816"
  project            = "gitlab-ci"
  project_id         = "gitlab-ci-155816"
  project_folder     = local.top_level_project_folder
  service_account_roles = [
    "roles/iam.serviceAccountCreator",
    "roles/cloudkms.admin"
  ]
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-ci-windows" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.2.2"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = "155816"
  project            = "gitlab-ci-windows"
  project_id         = "gitlab-ci-windows"
  project_folder     = local.top_level_project_folder
  service_account_roles = [
    "roles/iam.serviceAccountAdmin",
    "roles/iam.roleAdmin",
    "roles/resourcemanager.projectIamAdmin"
  ]
}

module "gitlab-org-ci" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.3.0"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account    = var.billing_account
  project            = "gitlab-org-ci"
  project_folder     = local.top_level_project_folder
  service_account_roles = [
    "roles/iam.serviceAccountAdmin",
    "roles/cloudkms.admin",
    "roles/iam.roleAdmin",
    "roles/resourcemanager.projectIamAdmin",
  ]
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-ops" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.2.2"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ops"],
    ),
  )
  billing_account       = var.billing_account
  bucket_name_prefix    = ""
  project               = "gitlab-ops"
  project_folder        = local.top_level_project_folder // gitlab.com/Infrastructure
  use_name_as_id        = "true"
}

module "gitlab-pre" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.2.2"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-pre"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-pre"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
}

module "gitlab-production" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.2.2"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-production"],
    ),
  )
  billing_account            = var.billing_account
  bucket_name_prefix         = ""
  project                    = "gitlab-production"
  project_folder             = local.project_folder
  serial_port_logging_enable = true
  use_name_as_id             = "true"
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-release" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.2.2"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-release"],
    ),
  )
  billing_account       = var.billing_account
  bucket_name_prefix    = ""
  project               = "gitlab-release"
  project_folder        = local.project_folder
  use_name_as_id        = "true"
}

module "gitlab-staging" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.2.2"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-staging"],
    ),
  )
  billing_account            = var.billing_account
  bucket_name_prefix         = ""
  project                    = "gitlab-staging"
  project_id                 = "gitlab-staging-1"
  project_folder             = local.project_folder
  serial_port_logging_enable = true
}

module "gitlab-testbed" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.2.2"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-testbed"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-testbed"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
}

module "gitlab-vault" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.2.2"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-vault"],
    ),
  )
  billing_account       = var.billing_account
  bucket_name_prefix    = ""
  project               = "gitlab-vault"
  project_folder        = local.project_folder
  use_name_as_id        = "true"
  service_account_roles = [
    "roles/container.admin",
    "roles/iam.serviceAccountAdmin",
    "roles/iam.roleAdmin",
    "roles/cloudkms.admin"
  ]
}

module "gitlab-vault-nonprod" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v7.2.2"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-vault-nonprod"],
    ),
  )
  billing_account       = var.billing_account
  bucket_name_prefix    = ""
  project               = "gitlab-vault-nonprod"
  project_folder        = local.project_folder
  use_name_as_id        = "true"
  service_account_roles = [
    "roles/container.admin",
    "roles/iam.serviceAccountAdmin",
    "roles/iam.roleAdmin",
    "roles/cloudkms.admin"
  ]
}
