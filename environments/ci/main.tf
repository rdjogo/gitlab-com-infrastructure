terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/ci/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 2.20.0"
}

provider "random" {
  version = "~> 2.2"
}

locals {
  qa_tunnel_service_account_email = "${var.qa_tunnel_service_account_id}@${var.project}.iam.gserviceaccount.com"
}

##################################
#
#  Network
#
##################################

# note: enable_flow_logs is required with the google-provider 2.x, despite
# being deprecated. it can be removed when upgrading to 3.x.
# see:
# * https://github.com/terraform-providers/terraform-provider-google/issues/4881
# * https://github.com/terraform-providers/terraform-provider-google/issues/5019

data "google_compute_network" "default_network" {
  name = "default"
}

resource "google_compute_subnetwork" "private_runners" {
  ip_cidr_range = "10.0.0.0/20"
  name          = "private-runners"
  network       = data.google_compute_network.default_network.self_link
  region        = var.region

  # required with google-provider 2.x, see comment above
  enable_flow_logs = true

  log_config {
    aggregation_interval = "INTERVAL_1_MIN"
    flow_sampling        = 0.01
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_subnetwork" "gitlab_shared_runners" {
  ip_cidr_range = "10.0.16.0/20"
  name          = "gitlab-shared-runners"
  network       = data.google_compute_network.default_network.self_link
  region        = var.region

  # required with google-provider 2.x, see comment above
  enable_flow_logs = true

  log_config {
    aggregation_interval = "INTERVAL_1_MIN"
    flow_sampling        = 0.01
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_subnetwork" "shared_runners" {
  ip_cidr_range = "10.0.32.0/20"
  name          = "shared-runners"
  network       = data.google_compute_network.default_network.self_link
  region        = var.region

  # required with google-provider 2.x, see comment above
  enable_flow_logs = true

  log_config {
    aggregation_interval = "INTERVAL_1_MIN"
    flow_sampling        = 0.01
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

##################################
#
#  VPC flow logs
#
##################################

resource "google_project_iam_binding" "project" {
  project = var.project
  role    = "roles/bigquery.dataEditor"

  members = [
    google_logging_project_sink.vpc_flow_logs.writer_identity,
  ]
}

resource "google_logging_project_sink" "vpc_flow_logs" {
  name        = "vpc_flow_logs"
  destination = "bigquery.googleapis.com/projects/${var.project}/datasets/vpc_flow_logs"

  filter = <<-EOT
  resource.type="gce_subnetwork"
  logName="projects/${var.project}/logs/compute.googleapis.com%2Fvpc_flows"
  EOT

  unique_writer_identity = true

  depends_on = [
    google_bigquery_dataset.vpc_flow_logs,
  ]
}

resource "google_bigquery_dataset" "vpc_flow_logs" {
  dataset_id    = "vpc_flow_logs"
  friendly_name = "VPC Flow Logs"
  description   = "This dataset holds VPC flow logs, imported via a stackdriver sink."
  location      = "US"

  # 14 days of retention
  default_table_expiration_ms = 1000*3600*24*14
}

##################################
#
#  NAT gateway
#
##################################

module "nat" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-nat.git?ref=v1.1.1"

  log_level        = "ALL"
  nat_ip_count     = 40
  nat_ports_per_vm = 256
  region           = var.region

  # Right now there is only the "default" network in ci.
  # We should change this to module.network.name once we manage ci networks in terraform.
  network_name = "default"

  # Prevents change in NAT+IP name schema in v1.0.0, which would cause downtime.
  nat_name    = "nat-default-us-east1"
  router_name = "nat-router-default-us-east1"
}

##################################
#
#  Object storage buckets
#
##################################

# hp: I created the buckets using this module, but unfortunately it always fails
# to create "gitlab-ci-assets" and "gitlab-ci-secrets", as gcs bucket names need
# to be globally unique and apparantly those names already exist somewhere on the
# world. So I removed the buckets from the tf state again and this needs to stay
# commented.
#
#module "gitlab_object_storage" {
#  environment                       = var.environment
#  service_account_email             = var.service_account_email
#  gcs_service_account_email         = var.gcs_service_account_email
#  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
#  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.0.0"
#}

##################################
#
#  Service Accounts
#
##################################

resource "google_service_account" "terraform" {
  account_id   = "qa-tunnel"
  display_name = "Service Account for QA Tunnel VM"
  project      = var.project
}

resource "google_storage_bucket_iam_binding" "secrets-binding" {
  bucket = "155816-gitlab-${var.environment}-secrets"
  role   = "roles/storage.objectViewer"

  members = [
    "serviceAccount:${local.qa_tunnel_service_account_email}",
    "serviceAccount:${var.service_account_email}",
  ]
}

# Create the bootstrap KMS key ring
resource "google_kms_key_ring" "bootstrap" {
  name     = "gitlab-${var.environment}-bootstrap"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = google_kms_key_ring.bootstrap.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

# Create the gitlab-secrets KMS key ring
resource "google_kms_key_ring" "gitlab-secrets" {
  name     = "gitlab-secrets"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "secrets" {
  key_ring_id = google_kms_key_ring.gitlab-secrets.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
    "serviceAccount:${local.qa_tunnel_service_account_email}",
  ]
}

resource "google_service_account" "stale-ci-cleaner" {
  account_id   = "stale-ci-cleaner"
  display_name = "Stale CI Runners Cleaner Account"
  description  = "Service account used by the script that cleans up stale CI runners"
  # https://ops.gitlab.net/gitlab-com/gl-infra/ci-project-cleaner
}

resource "google_project_iam_custom_role" "stale-ci-cleaner" {
  role_id     = "staleCiCleaner"
  title       = "Stale CI Runners Cleaner role"
  description = "Role for the script that cleans up stale CI runners"
  permissions = var.stale_ci_cleaner_permissions
}

resource "google_project_iam_member" "stale-ci-cleaner" {
  role   = google_project_iam_custom_role.stale-ci-cleaner.id
  member = "serviceAccount:${google_service_account.stale-ci-cleaner.email}"
}

##############################
#
# Monitoring
#
##############################

module "sd-exporter" {
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  machine_type              = var.machine_types["sd-exporter"]
  name                      = "sd-exporter"
  node_count                = var.node_count["sd-exporter"]
  project                   = var.project
  public_ports              = var.public_ports["sd-exporter"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name           = "default"
  tier                      = "inf"
  use_new_node_name         = true
  use_external_ip           = true
  vpc                       = "default"
}

#######################################################
#
# Tenable.IO local Nessus scanner
#
#######################################################

module "nessus" {
  bootstrap_version     = 8
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-nessus]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  machine_type          = var.machine_types["nessus"]
  name                  = "nessus"
  node_count            = var.node_count["nessus"]
  project               = var.project
  public_ports          = var.public_ports["nessus"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name       = "default"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "default"
  use_external_ip       = true
}

# We want only 1 logs-based metric per project, not per NAT. This way, the
# stackdriver metric (and exported prometheus metric) will have a static name,
# with different NAT instances being identified by the "nat_name" label. Note
# that Stackdriver already applies a "gateway_name" label so we don't need to
# add our own.
resource "google_logging_metric" "errors" {
  name = "nat-errors"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="DROPPED"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

resource "google_logging_metric" "errors_by_vm" {
  name = "nat-errors-by-vm"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="DROPPED"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
    labels {
      key         = "vm_name"
      value_type  = "STRING"
    }
  }

  label_extractors = {
    "vm_name" = "EXTRACT(jsonPayload.endpoint.vm_name)"
  }
}

resource "google_logging_metric" "translations" {
  name = "nat-translations"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="OK"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}
