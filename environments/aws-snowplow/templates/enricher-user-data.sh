#!/bin/bash
## This has, so far, been written to run on Amazon Linux 2 AMI.

## Add nessus user and key
groupadd ${nessus-user}
adduser -m -s /bin/bash ${nessus-user} -g wheel -g ${nessus-user}
mkdir -p /home/${nessus-user}/.ssh
chmod 700 /home/${nessus-user}/.ssh
echo -e "${nessus-key}" > /home/${nessus-user}/.ssh/authorized_keys
chmod 600 /home/${nessus-user}/.ssh/authorized_keys
chown -R ${nessus-user}:${nessus-user} /home/${nessus-user}/.ssh
echo "${nessus-user} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/99-${nessus-user}-access
chmod 440 /etc/sudoers.d/99-${nessus-user}-access

## Install Java 1.8
yum -y install java-1.8.0-openjdk
## Set up user, group, and install location
groupadd snowplow
adduser --system --gid snowplow snowplow
mkdir -p /snowplow
mkdir -p /snowplow/config
mkdir -p /snowplow/enrichments
mkdir -p /snowplow/logs
## Install SnowPlow Kinesis Collector
mkdir -p /tmp/build
cd /tmp/build
wget -q http://dl.bintray.com/snowplow/snowplow-generic/snowplow_stream_enrich_kinesis_0.21.0.zip
unzip -d /snowplow/bin snowplow_stream_enrich_kinesis_0.21.0.zip
cd /tmp
rm -rf /tmp/build
## We need an IGLU Resolver config
cat > /snowplow/config/iglu_resolver.json <<EOJ
{
  "schema": "iglu:com.snowplowanalytics.iglu/resolver-config/jsonschema/1-0-2",
  "data": {
    "cacheSize": 2000,
    "cacheTtl": 3600,
    "repositories": [
      {
        "name": "Iglu Central",
        "priority": 0,
        "vendorPrefixes": [
          "com.snowplowanalytics"
        ],
        "connection": {
          "http": {
            "uri": "http://iglucentral.com"
          }
        }
      },
      {
        "name": "Iglu Central - GCP Mirror",
        "priority": 1,
        "vendorPrefixes": [ "com.snowplowanalytics" ],
        "connection": {
          "http": {
            "uri": "http://mirror01.iglucentral.com"
          }
        }
      },
      {
        "name": "Iglu Gitlab",
        "priority": 0,
        "vendorPrefixes": [
          "com.gitlab"
        ],
        "connection": {
          "http": {
            "uri": "https://gitlab-org.gitlab.io/iglu"
          }
        }
      }
    ]
  }
}
EOJ
## IP Lookup Encrichment
cat > /snowplow/enrichments/ip_lookups.json <<EOR
{
  "schema": "iglu:com.snowplowanalytics.snowplow/ip_lookups/jsonschema/2-0-0",

  "data": {

    "name": "ip_lookups",
    "vendor": "com.snowplowanalytics.snowplow",
    "enabled": true,
    "parameters": {
      "geo": {
        "database": "GeoLite2-City.mmdb",
        "uri": "http://snowplow-hosted-assets.s3.amazonaws.com/third-party/maxmind"
      }
    }
  }
}
EOR
## user_agent_utils_config Enrichment
cat > /snowplow/enrichments/user_agent_utils_config.json <<EOA
{
  "schema": "iglu:com.snowplowanalytics.snowplow/user_agent_utils_config/jsonschema/1-0-0",
  "data": {
    "vendor": "com.snowplowanalytics.snowplow",
    "name": "user_agent_utils_config",
    "enabled": true,
    "parameters": {}
  }
}
EOA
## Need to copy in a config
cat > /snowplow/config/enricher.hocon <<EOF
enrich {
  production = true

  streams {
    in {
      raw = "snowplow-raw-good"
    }

    out {
      enriched = "snowplow-enriched-good"
      bad = "snowplow-enriched-bad"
      partitionKey = "user_ipaddress"
    }

    sourceSink {
      enabled = kinesis

      aws {
        accessKey = iam
        secretKey = iam
      }

      region = "us-east-1"
      maxRecords = 10000
      initialPosition = TRIM_HORIZON
      backoffPolicy {
        minBackoff = 10
        maxBackoff = 300000
      }
    }

    buffer {
      byteLimit = 16384
      recordLimit = 1000
      timeLimit = 10000
    }

    appName = "SnowplowEnrich-gitlab-us-east-1"
  }
}
EOF
chown -R snowplow:snowplow /snowplow
## Star the collector service
su snowplow -g snowplow -c 'nohup /usr/bin/java -jar /snowplow/bin/snowplow-stream-enrich-kinesis-0.21.0.jar --config /snowplow/config/enricher.hocon --enrichments file:/snowplow/enrichments --resolver file:/snowplow/config/iglu_resolver.json > /snowplow/logs/out.log 2>&1 &'
