#!/bin/bash
## This has, so far, been written to run on Amazon Linux 2 AMI.

## Add nessus user and key
groupadd ${nessus-user}
adduser -m -s /bin/bash ${nessus-user} -g wheel -g ${nessus-user}
mkdir -p /home/${nessus-user}/.ssh
chmod 700 /home/${nessus-user}/.ssh
echo -e "${nessus-key}" > /home/${nessus-user}/.ssh/authorized_keys
chmod 600 /home/${nessus-user}/.ssh/authorized_keys
chown -R ${nessus-user}:${nessus-user} /home/${nessus-user}/.ssh
echo "${nessus-user} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/99-${nessus-user}-access
chmod 440 /etc/sudoers.d/99-${nessus-user}-access

## Install Java 1.8
yum -y install java-1.8.0-openjdk
## Set up user, group, and install location
groupadd snowplow
adduser --system --gid snowplow snowplow
mkdir -p /snowplow
mkdir -p /snowplow/config
mkdir -p /snowplow/logs
## Install SnowPlow Kinesis Collector
mkdir -p /tmp/build
cd /tmp/build
wget -q http://dl.bintray.com/snowplow/snowplow-generic/snowplow_scala_stream_collector_kinesis_0.15.0.zip
unzip -d /snowplow/bin snowplow_scala_stream_collector_kinesis_0.15.0.zip
cd /tmp
rm -rf /tmp/build
## Need to copy in a config
cat > /snowplow/config/collector.hocon <<EOF
collector {
  interface = "0.0.0.0"
  port = "8000"
  production = true

  p3p {
    policyRef = "/w3c/p3p.xml"
    CP = "NOI DSP COR NID PSA OUR IND COM NAV STA"
  }

  crossDomain {
    enabled = true
    domains = [ "*" ]
    secure = true
  }

  cookie {
    enabled = false
    expiration = "365 days"
    name = "snwplw"
    domain = "gitlab.sinter-collect.com"
  }

  doNotTrackCookie {
    enabled = false
    name = "COLLECTOR_DO_NOT_TRACK_COOKIE_NAME"
    value = "COLLECTOR_DO_NOT_TRACK_COOKIE_VALUE"
  }

  cookieBounce {
    enabled = false
    name = "n3pc"
    fallbackNetworkUserId = "00000000-0000-4000-A000-000000000000"
    forwardedProtocolHeader = "X-Forwarded-Proto"
  }

  redirectMacro {
    enabled = false
  }

  cors {
    accessControlMaxAge = 5 seconds
  }

  rootResponse {
    enabled = false
    statusCode = 302
  }

  prometheusMetrics {
    enabled = false
  }

  streams {
    good = "snowplow-raw-good"
    bad = "snowplow-raw-bad"
    useIpAddressAsPartitionKey = true

    sink {
      enabled = kinesis
      region = "us-east-1"
      threadPoolSize = 10

      aws {
        accessKey = iam
        secretKey = iam
      }

      backoffPolicy {
        minBackoff = 10
        maxBackoff = 300000
      }
    }

    buffer {
      byteLimit = 16384
      recordLimit = 1000
      timeLimit = 10000
    }
  }
}

akka {
  loglevel = OFF
  loggers = ["akka.event.slf4j.Slf4jLogger"]

  http.server {
    remote-address-header = on
    raw-request-uri-header = on

    parsing {
      max-uri-length = 32768
      uri-parsing-mode = relaxed
    }
  }
}
EOF
chown -R snowplow:snowplow /snowplow
## Star the collector service
su snowplow -g snowplow -c 'nohup /usr/bin/java -jar /snowplow/bin/snowplow-stream-collector-kinesis-0.15.0.jar --config /snowplow/config/collector.hocon > /snowplow/logs/out.log 2>&1 &'
