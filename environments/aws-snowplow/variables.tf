variable "nessus-user" {
  description = "Nessus username"
  default     = "nessus-aws"
}

variable "nessus-key" {
  description = "Nessus user public key"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCrDa2zYa2RGYG+GDww1WtChXYhoiHgJyqpQRp5QT82UwUlxk2gUF0mOvTCL3j47U0EWn6i3wQyi37cwYS3Ow4BojXWG1fDpphnvohyAHcndq3w75yPXBu09wiuNvvtGveRtNxeT9+0CQYZLNQqh3neFvWUTjfdQH/oj50sn/qC2g7zm7xt7vjPyOiYLqV3avWexsuokGPx9RcAcngU848a6eNMK8I4/BMWbNt9PBA3hYcWwhx5lDvOJCmpTQXkzddyqExCX2ImG6S3dbmzTHdtjvgWganI7af21jffURBAM1yJeEjwb5KS5nY07PKfssCg6ZY6pn7VudcpV7xspbKr ops-contact+nessus-aws@gitlab.com"
}
