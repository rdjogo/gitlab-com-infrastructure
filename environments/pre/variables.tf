## These variables must be set in env secrets!

variable "oauth2_client_id_monitoring" {
}

variable "oauth2_client_secret_monitoring" {
}

variable "gitlab_io_zone_id" {}
variable "gitlab_static_net_zone_id" {}

variable "project" {
  default = "gitlab-pre"
}

variable "bootstrap_script_version" {
  default = 8
}

variable "region" {
  default = "us-east1"
}

variable "environment" {
  default = "pre"
}

variable "dns_zone_name" {
  default = "gitlab.net"
}

variable "machine_types" {
  type = map(string)

  default = {
    "bastion"            = "n1-standard-1"
    "monitoring"         = "n1-standard-2"
    "sd-exporter"        = "n1-standard-1"
    "gitlab-gke"         = "n1-standard-4"
    "gitlab-gke-sidekiq" = "n1-standard-1"
    "web"                = "n1-standard-1"
    "git"                = "n1-standard-1"
    "api"                = "n1-standard-1"
    "sidekiq-catchall"   = "n1-standard-1"
    "web-pages"          = "n1-standard-1"
    "fe-lb"              = "n1-standard-1"
    "gitaly"             = "n1-standard-1"
    "deploy"             = "n1-standard-1"
    "consul"             = "n1-standard-1"
    "thanos"             = "n1-standard-1"
    "praefect"           = "n1-standard-1"
    "praefect-db"        = "db-f1-micro"
  }
}

variable "monitoring_hosts" {
  type = map(list(string))

  default = {
    "names" = ["prometheus", "prometheus-app"]
    "ports" = [9090, 9090]
  }
}

variable "service_account_email" {
  type = string

  default = "terraform@gitlab-pre.iam.gserviceaccount.com"
}

#############################
# Default firewall
# rule for allowing
# all protocols on all
# ports
#
# 10.232.x.x: all of pre
# 10.250.7.x: ops runner
# 10.250.10.x: chatops runner
# 10.250.12.x: release runner
# 10.12.0.0/14: pod address range in gitlab-ops for runners
###########################

variable "internal_subnets" {
  type    = list(string)
  default = ["10.232.0.0/13", "10.250.7.0/24", "10.250.10.0/24", "10.250.12.0/24", "10.12.0.0/14"]
}

variable "other_monitoring_subnets" {
  type = list(string)

  # 10.219.1.0/24: gprd
  # 10.251.17.0/24: dr
  # Left empty for preprod
  default = []
}

# The pre network is allocated
# 10.232.0.0/13
#   First IP: 10.232.0.0
#   Last IP:  10.239.255.255

variable "subnetworks" {
  type = map(string)

  default = {
    "bastion"                 = "10.232.1.0/24"
    "monitoring"              = "10.232.3.0/24"
    "fe-lb"                   = "10.232.9.0/24"
    "fe-lb-pages"             = "10.232.10.0/24"
    "fe-lb-registry"          = "10.232.11.0/24"
    "web"                     = "10.232.13.0/24"
    "api"                     = "10.232.14.0/24"
    "git"                     = "10.232.15.0/24"
    "sidekiq"                 = "10.232.16.0/24"
    "web-pages"               = "10.232.17.0/24"
    "gitaly"                  = "10.232.18.0/24"
    "deploy"                  = "10.232.19.0/24"
    "gitlab-gke"              = "10.232.20.0/24"
    "consul"                  = "10.232.22.0/24"
    "pubsubbeat"              = "10.232.23.0/24"
    "thanos-compact"          = "10.232.25.0/24"
    "thanos-store"            = "10.232.26.0/24"
    "praefect"                = "10.232.27.0/24"
    "gitlab-gke-pod-cidr"     = "10.235.0.0/16"
    "gitlab-gke-service-cidr" = "10.236.0.0/16"
    # /mnt/storage
    "filestore-storage" = "10.237.0.0/29"
  }
}

variable "master_cidr_subnets" {
  type = map(string)

  default = {
    "gitlab-gke" = "172.16.0.0/28"
  }
}

##################
# Network Peering
##################

variable "network_env" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-pre/global/networks/pre"
}

variable "peer_networks" {
  type = map(list(string))

  default = {
    "names" = ["ops", "vault-nonprod"]
    "links" = [
      "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops",
      "https://www.googleapis.com/compute/v1/projects/gitlab-vault-nonprod/global/networks/vault",
    ]
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "bastion"     = [22]
    "sd-exporter" = []
    "pubsubbeat"  = []
    "web"         = []
    "api"         = []
    "git"         = []
    "sidekiq"     = []
    "web-pages"   = []
    "fe-lb"       = [22, 80, 443]
    "gitaly"      = []
    "deploy"      = []
    "consul"      = []
    "thanos"      = []
    "praefect"    = []
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "bastion"          = 1
    "prometheus"       = 1
    "prometheus-app"   = 1
    "sd-exporter"      = 1
    "web"              = 1
    "git"              = 1
    "api"              = 1
    "sidekiq-catchall" = 1
    "web-pages"        = 1
    "fe-lb"            = 1
    "fe-lb-pages"      = 1
    "fe-lb-registry"   = 1
    "gitaly"           = 2
    "deploy"           = 1
    "consul"           = 1
    "thanos"           = 1
    "praefect"         = 1
  }
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-pre-chef-bootstrap"
    bootstrap_key     = "gitlab-pre-bootstrap-validation"
    bootstrap_keyring = "gitlab-pre-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "data_disk_sizes" {
  type = map(string)

  default = {
    "file"  = "100"
    "share" = "100"
    "pages" = "100"
  }
}

variable "lb_fqdns" {
  type    = list(string)
  default = ["test.pre.gitlab.com", "pre.gitlab.com"]
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.pre.gitlab.com"]
}

variable "lb_fqdns_pages" {
  type    = list(string)
  default = ["*.pages.pre.gitlab.io"]
}

variable "lb_fqdns_registry" {
  type    = list(string)
  default = ["registry-test.pre.gitlab.com", "registry.pre.gitlab.com"]
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "tcp_lbs_pages" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_registry" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_sentry" {
  type = map(list(string))

  default = {
    "names"                      = ["http", "https"]
    "forwarding_port_ranges"     = ["80", "443"]
    "health_check_ports"         = ["9000", "9000"]
    "health_check_request_paths" = ["/auth/login/gitlab/", "/auth/login/gitlab/"]
  }
}

variable "tcp_lbs" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https", "ssh"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-pre.iam.gserviceaccount.com"
}

variable "egress_ports" {
  type    = list(string)
  default = ["80", "443"]
}

variable "deploy_egress_ports" {
  type    = list(string)
  default = ["80", "443", "22"]
}

#######################
# pubsubbeat config
#######################

variable "pubsubbeats" {
  type = map(list(string))

  default = {
    "names" = [
      "api",
      "application",
      "consul",
      "gitaly",
      "gitlab-shell",
      "gke",
      "haproxy",
      "nginx",
      "pages",
      "production",
      "rails",
      "runner",
      "shell",
      "sidekiq",
      "system",
      "unicorn",
      "unstructured",
      "workhorse",
      "monitoring",
    ]
    "machine_types" = [
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
      "n1-standard-1",
    ]
  }
}

### Object Storage Configuration

variable "versioning" {
  type    = string
  default = "true"
}

variable "artifact_age" {
  type    = string
  default = "30"
}

variable "upload_age" {
  type    = string
  default = "30"
}

variable "lfs_object_age" {
  type    = string
  default = "30"
}

variable "package_repo_age" {
  type    = string
  default = "30"
}

variable "storage_class" {
  type    = string
  default = "MULTI_REGIONAL"
}

variable "storage_log_age" {
  type    = string
  default = "7"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

#################
# Monitoring whitelist
#################

#################
# Allow traffic from the ops
# network from grafana
#################

variable "monitoring_whitelist_prometheus" {
  type = map(list(string))

  default = {
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    # 10.250.8.x for the ops prometheus servers
    #
    # Port 9090 is for prometheus
    # Ports 10900-10902 is for thanos
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]
    "ports"   = ["9090", "10900", "10901", "10902"]
  }
}

variable "monitoring_whitelist_thanos" {
  type = map(list(string))

  default = {
    # 10.250.8.x for the ops prometheus servers
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]
    "ports"   = ["10901", "10902"]
  }
}

#################
# Allow traffic from the ops
# network from the alerts manager
#################
variable "monitoring_whitelist_alerts" {
  type = map(list(string))

  default = {
    # 10.250.8.x for the ops alerts servers
    "subnets" = ["10.250.8.0/24"]
    "ports"   = ["9093"]
  }
}

####################################
# Default log filters for stackdriver
#####################################

variable "sd_log_filters" {
  type = map(string)

  default = {
    "exclude_logtypes" = "resource.type=\"gce_instance\" AND labels.tag!=\"haproxy\""
  }
}

variable "cloudflare_api_key" {}
variable "cloudflare_account_id" {}
variable "cloudflare_email" {}
