## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/pre/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

## CloudFlare

provider "cloudflare" {
  version    = "= 2.3"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

## Local
provider "local" {
  version = "~> 1.4.0"
}

variable "gitlab_com_zone_id" {
}

variable "gitlab_net_zone_id" {
}

## Google

provider "google" {
  version = "~> 2.20.0"
  project = var.project
  region  = var.region
}

provider "google-beta" {
  version = "~> 3.26.0"
  project = var.project
  region  = var.region
}

##################################
#
#  Network
#
#################################

module "network" {
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.0"
  project          = var.project
  environment      = var.environment
  internal_subnets = var.internal_subnets
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering" {
  count        = length(var.peer_networks["names"])
  name         = "peering-${element(var.peer_networks["names"], count.index)}"
  network      = var.network_env
  peer_network = element(var.peer_networks["links"], count.index)
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = var.subnetworks["monitoring"]
  name                     = format("monitoring-%v", var.environment)
  network                  = module.network.self_link
  private_ip_google_access = true
  project                  = var.project
  region                   = var.region
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  environment     = var.environment
  dns_zone_id     = var.gitlab_net_zone_id
  dns_zone_name   = "gitlab.net"
  dns_zone_prefix = "pre."
  hosts           = var.monitoring_hosts["names"]
  name            = "monitoring-lb"
  project         = var.project
  region          = var.region
  service_ports   = var.monitoring_hosts["ports"]
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v3.0.1"
  subnetwork_name = google_compute_subnetwork.monitoring.name
  targets         = var.monitoring_hosts["names"]
  url_map         = google_compute_url_map.monitoring-lb.self_link
}

#######################
module "prometheus" {
  bootstrap_version = 6
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size    = 50
  data_disk_type    = "pd-ssd"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus"
  node_count            = var.node_count["prometheus"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  public_ports          = ["22"]
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/graph"
  service_port = element(
    var.monitoring_hosts["ports"],
    index(var.monitoring_hosts["names"], "prometheus"),
  )
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v4.2.1"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "prometheus-app" {
  bootstrap_version = 6
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus-app]\""
  data_disk_size    = 50
  data_disk_type    = "pd-ssd"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus-app"
  node_count            = var.node_count["prometheus-app"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/graph"
  service_port = element(
    var.monitoring_hosts["ports"],
    index(var.monitoring_hosts["names"], "prometheus-app"),
  )
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v4.2.1"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "sd-exporter" {
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = 6
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  machine_type              = var.machine_types["sd-exporter"]
  name                      = "sd-exporter"
  node_count                = var.node_count["sd-exporter"]
  project                   = var.project
  public_ports              = var.public_ports["sd-exporter"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name           = google_compute_subnetwork.monitoring.name
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

###############################################
#
# Load balancer and VM for the pre bastion
#
###############################################

module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

module "bastion" {
  bootstrap_version     = 6
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Consul
#
##################################

module "consul" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-consul]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["consul"]
  machine_type          = var.machine_types["consul"]
  name                  = "consul"
  node_count            = var.node_count["consul"]
  project               = var.project
  public_ports          = var.public_ports["consul"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 8300
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Google storage buckets
#
##################################

module "storage" {
  environment                       = var.environment
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.3.0"
  secrets_viewer_access = [
    "serviceAccount:${google_service_account.k8s-workloads-ro.email}",
    "serviceAccount:${google_service_account.k8s-workloads.email}"
  ]
}

##################################
#
#  GKE Cluster for pre.gitlab.com GitLab services
#
##################################

# Service account utilized by CI
resource "google_service_account" "k8s-workloads" {
  account_id   = "k8s-workloads"
  display_name = "k8s-workloads"
}

# Service account utilized by CI for Read Only operations
resource "google_service_account" "k8s-workloads-ro" {
  account_id   = "k8s-workloads-ro"
  display_name = "k8s-workloads-ro"
}

resource "google_project_iam_custom_role" "k8s-workloads" {
  project     = var.project
  role_id     = "k8sWorkloads"
  title       = "k8s-workloads"
  permissions = ["clientauthconfig.clients.listWithSecrets"]
}

resource "google_project_iam_binding" "k8s-workloads" {
  project = var.project
  role    = "projects/${var.project}/roles/${google_project_iam_custom_role.k8s-workloads.role_id}"

  members = [
    "serviceAccount:${google_service_account.k8s-workloads.email}",
    "serviceAccount:${google_service_account.k8s-workloads-ro.email}"
  ]
}

# IP address for NAT
resource "google_compute_address" "gke-cloud-nat-ip" {
  name        = "gitlab-gke"
  description = "gitlab-gke"
}

# Reserved IP address for container registry
resource "google_compute_address" "registry-gke" {
  name         = "registry-gke-${var.environment}"
  description  = "registry-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for prometheus operator
resource "google_compute_address" "prometheus-gke" {
  name         = "prometheus-gke-${var.environment}"
  description  = "prometheus-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for alertmanager ingress with IAP
resource "google_compute_global_address" "alertmanager-ingress-iap" {
  name        = "alertmanager-ingress-iap-${var.environment}"
  description = "alertmanager-ingress-iap-${var.environment}"
}

module "alertmanager-gke-pre-gitlab-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "alertmanager-gke.pre.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.alertmanager-ingress-iap.address]
    }
  }
}

# Reserved IP address for prometheus ingress with IAP
resource "google_compute_global_address" "prometheus-ingress-iap" {
  name        = "prometheus-ingress-iap-${var.environment}"
  description = "prometheus-ingress-iap-${var.environment}"
}

module "prometheus-gke-pre-gitlab-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "prometheus-gke.pre.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.prometheus-ingress-iap.address]
    }
  }
}

# Reserved IP address for thanos-query
resource "google_compute_address" "thanos-query-gke" {
  name         = "thanos-query-gke-${var.environment}"
  description  = "thanos-query-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for plantuml, must be global

resource "google_compute_global_address" "plantuml-gke" {
  name        = "plantuml-gke-${var.environment}"
  description = "plantuml-gke-${var.environment}"
}

module "pre-plantuml-gitlab-static-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab-static.net."

  a = {
    "pre.plantuml.gitlab-static.net." = {
      ttl     = "300"
      records = [google_compute_global_address.plantuml-gke.address]
    }
  }
}

resource "google_compute_router" "nat-router" {
  name    = "gitlab-gke"
  network = module.network.self_link
}

resource "google_compute_router_nat" "gke-nat" {
  name                               = "gitlab-gke"
  router                             = google_compute_router.nat-router.name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.gke-cloud-nat-ip.self_link]
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = module.gitlab-gke.subnetwork_self_link
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

module "gitlab-gke" {
  environment              = var.environment
  name                     = "gitlab-gke"
  vpc                      = module.network.self_link
  source                   = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v9.0.1"
  enable_workload_identity = "true"
  ip_cidr_range            = var.subnetworks["gitlab-gke"]
  disable_network_policy   = "false"
  dns_zone_name            = var.dns_zone_name
  kubernetes_version       = "1.16.10-gke.8"
  node_network_policy      = "true"
  private_cluster          = "true"
  private_master_cidr      = var.master_cidr_subnets["gitlab-gke"]
  project                  = var.project
  region                   = var.region
  pod_ip_cidr_range        = var.subnetworks["gitlab-gke-pod-cidr"]
  service_ip_cidr_range    = var.subnetworks["gitlab-gke-service-cidr"]

  service_account = "default"

  node_pools = {
    "default-pool-0" = {
      initial_node_count = "1"
      machine_type       = var.machine_types["gitlab-gke"]
      max_node_count     = "5"
      node_auto_repair   = "true"
      node_auto_upgrade  = "false"
      node_disk_size_gb  = "50"
      node_disk_type     = "pd-standard"
      preemptible        = "true"
      type               = "default"
    }
    "sidekiq-memory-bound-0" = {
      initial_node_count = "1"
      machine_type       = var.machine_types["gitlab-gke-sidekiq"]
      max_node_count     = "3"
      node_auto_repair   = "true"
      node_auto_upgrade  = "false"
      node_disk_size_gb  = "50"
      node_disk_type     = "pd-standard"
      preemptible        = "true"
      type               = "memory-bound"
    }
    "sidekiq-low-urgency-cpu-bound-0" = {
      initial_node_count = "1"
      machine_type       = var.machine_types["gitlab-gke-sidekiq"]
      max_node_count     = "3"
      node_auto_repair   = "true"
      node_auto_upgrade  = "false"
      node_disk_size_gb  = "50"
      node_disk_type     = "pd-standard"
      preemptible        = "true"
      type               = "low-urgency-cpu-bound"
    }
    "sidekiq-urgent-other-0" = {
      initial_node_count = "1"
      machine_type       = var.machine_types["gitlab-gke-sidekiq"]
      max_node_count     = "3"
      node_auto_repair   = "true"
      node_auto_upgrade  = "false"
      node_disk_size_gb  = "50"
      node_disk_type     = "pd-standard"
      preemptible        = "true"
      type               = "urgent-other"
    }
    "sidekiq-urgent-cpu-bound-0" = {
      initial_node_count = "1"
      machine_type       = var.machine_types["gitlab-gke-sidekiq"]
      max_node_count     = "3"
      node_auto_repair   = "true"
      node_auto_upgrade  = "false"
      node_disk_size_gb  = "50"
      node_disk_type     = "pd-standard"
      preemptible        = "true"
      type               = "urgent-cpu-bound"
    }
  }
}

// Allow GKE to talk to the prometheus operator which utilizes port 8443
resource "google_compute_firewall" "gke-master-to-kubelet" {
  name    = "k8s-api-to-kubelets"
  network = module.network.self_link
  project = var.project

  description = "GKE API to kubelets"

  source_ranges = [var.master_cidr_subnets["gitlab-gke"]]

  allow {
    protocol = "tcp"
    ports    = ["8443"]
  }

  target_tags = ["gitlab-gke"]
}

##################################
#
#  Pubsubbeat for GKE
#  This adds a single beat for logs
#  from GKE
#
#  Machines for running the beats
#  that consume logs from pubsub
#  and send them to elastic cloud
#
#  You must have a chef role with the
#  following format:
#     role[<env>-infra-pubsubbeat-<beat_name>]
#
##################################

module "pubsubbeat" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-pubsub]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  gke_log_filter        = "protoPayload.authenticationInfo:* OR protoPayload.method:* OR protoPayload.requestMetadata:*"
  health_check          = "tcp"
  machine_types         = var.pubsubbeats["machine_types"]
  names                 = var.pubsubbeats["names"]
  ip_cidr_range         = var.subnetworks["pubsubbeat"]
  project               = var.project
  public_ports          = var.public_ports["pubsubbeat"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/pubsubbeat.git?ref=v6.1.1"
  tier                  = "inf"
  topic_names = formatlist(
    "%v-%v-%v-%v",
    "pubsub",
    var.pubsubbeats["names"],
    "inf",
    var.environment,
  )
  use_new_node_name = true
  vpc               = module.network.self_link
}

## bindings for pubsubbeat in k8s
resource "google_service_account" "pubsubbeat-k8s-sa" {
  account_id   = "pubsubbeat-k8s"
  display_name = "pubsubbeat-k8s"
  description  = "Service account used by pubsubbeat in k8s for subscribing to PubSub"
}

# Updates an IAM policy to add the kubernetes service account pubsubbeat/es-diagnostics to the GCP service account in order to be able to use GKE Workload Identity
resource "google_service_account_iam_member" "pubsubbeat-pubsubbeat" {
  role               = "roles/iam.workloadIdentityUser"
  service_account_id = google_service_account.pubsubbeat-k8s-sa.name
  member             = "serviceAccount:${var.project}.svc.id.goog[pubsubbeat/pubsubbeat]"
}

resource "google_project_iam_member" "pubsubbeat-pubsubbeat-pubsub-subscriber" {
  project = var.project
  role    = "roles/pubsub.subscriber"
  member  = "serviceAccount:${google_service_account.pubsubbeat-k8s-sa.email}"
}

resource "google_project_iam_member" "pubsubbeat-pubsubbeat-pubsub-editor" {
  project = var.project
  role    = "roles/pubsub.editor"
  member  = "serviceAccount:${google_service_account.pubsubbeat-k8s-sa.email}"
}

##################################
#
#  External HAProxy LoadBalancer
#
##################################

module "fe-lb" {
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-fe]\""
  create_backend_service = false
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe"
  node_count             = var.node_count["fe-lb"]
  os_boot_image          = "ubuntu-os-cloud/ubuntu-1804-lts"
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer Pages
#
##################################

module "fe-lb-pages" {
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-pages]\""
  create_backend_service = false
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb-pages"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe-pages"
  node_count             = var.node_count["fe-lb-pages"]
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_port           = 7331
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer Registry
#
##################################

module "fe-lb-registry" {
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-registry]\""
  create_backend_service = false
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb-registry"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe-registry"
  node_count             = var.node_count["fe-lb-registry"]
  project                = var.project
  os_boot_image          = "ubuntu-os-cloud/ubuntu-1804-lts"
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  GCP TCP LoadBalancers
#
##################################

#### Load balancer for the main site
module "gcp-tcp-lb" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs["health_check_ports"]
  instances              = module.fe-lb.instances_self_link
  lb_count               = length(var.tcp_lbs["names"])
  name                   = "gcp-tcp-lb"
  names                  = var.tcp_lbs["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["fe"]
}

#### Load balancer for pages
module "gcp-tcp-lb-pages" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_pages["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_pages
  gitlab_zone            = "gitlab.io."
  health_check_ports     = var.tcp_lbs_pages["health_check_ports"]
  instances              = module.fe-lb-pages.instances_self_link
  lb_count               = length(var.tcp_lbs_pages["names"])
  name                   = "gcp-tcp-lb-pages"
  names                  = var.tcp_lbs_pages["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["fe-pages"]
}

#### Load balancer for registry
module "gcp-tcp-lb-registry" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_registry["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_registry
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_registry["health_check_ports"]
  instances              = module.fe-lb-registry.instances_self_link
  lb_count               = length(var.tcp_lbs_registry["names"])
  name                   = "gcp-tcp-lb-registry"
  names                  = var.tcp_lbs_registry["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["fe-registry"]
}

##################################
#
#  Deploy
#
##################################

module "deploy" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-deploy-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.deploy_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["deploy"]
  machine_type          = var.machine_types["deploy"]
  name                  = "deploy"
  node_count            = var.node_count["deploy"]
  project               = var.project
  public_ports          = var.public_ports["deploy"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Web front-end
#
#################################

module "web" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web"]
  machine_type          = var.machine_types["web"]
  name                  = "web"
  node_count            = var.node_count["web"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  API
#
#################################

module "api" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-api]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["api"]
  machine_type          = var.machine_types["api"]
  name                  = "api"
  node_count            = var.node_count["api"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["api"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Git
#
##################################

module "git" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-git]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["git"]
  machine_type          = var.machine_types["git"]
  name                  = "git"
  node_count            = var.node_count["git"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["git"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Pages web front-end
#
#################################

module "web-pages" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web-pages]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web-pages"]
  machine_type          = var.machine_types["web-pages"]
  name                  = "web-pages"
  node_count            = var.node_count["web-pages"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web-pages"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Sidekiq
#
##################################

module "sidekiq" {
  bootstrap_version                           = var.bootstrap_script_version
  chef_provision                              = var.chef_provision
  chef_run_list                               = "\"role[${var.environment}-base-be-sidekiq-catchall]\""
  dns_zone_name                               = var.dns_zone_name
  environment                                 = var.environment
  ip_cidr_range                               = var.subnetworks["sidekiq"]
  machine_type                                = var.machine_types["sidekiq-catchall"]
  name                                        = "sidekiq"
  os_disk_type                                = "pd-ssd"
  project                                     = var.project
  public_ports                                = var.public_ports["sidekiq"]
  region                                      = var.region
  service_account_email                       = var.service_account_email
  sidekiq_elasticsearch_count                 = "0"
  sidekiq_elasticsearch_instance_type         = "none"
  sidekiq_low_urgency_cpu_bound_count         = "0"
  sidekiq_low_urgency_cpu_bound_instance_type = "none"
  sidekiq_catchall_count                      = var.node_count["sidekiq-catchall"]
  sidekiq_catchall_instance_type              = var.machine_types["sidekiq-catchall"]
  sidekiq_urgent_other_count                  = "0"
  sidekiq_urgent_other_instance_type          = "none"
  sidekiq_urgent_cpu_bound_count              = "0"
  sidekiq_urgent_cpu_bound_instance_type      = "none"
  source                                      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-sidekiq.git?ref=v7.0.0"
  tier                                        = "sv"
  use_new_node_name                           = true
  vpc                                         = module.network.self_link
}

##################################
#
#  Gitaly Praefect nodes
#
##################################

module "praefect" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-be-praefect]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "http"
  health_check_port     = 9652
  ip_cidr_range         = var.subnetworks["praefect"]
  machine_type          = var.machine_types["praefect"]
  name                  = "praefect"
  node_count            = var.node_count["praefect"]
  project               = var.project
  public_ports          = var.public_ports["praefect"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

module "praefect-db" {
  availability_type = "REGIONAL" # REGIONAL enables HA
  database_version  = "POSTGRES_11"
  name              = "praefect-db"
  project           = var.project
  region            = var.region
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-sql.git?ref=v2.0.0"
  tier              = var.machine_types["praefect-db"]
  # Note: Network peering can be moody. If your `apply` fails to create the
  # `private_vpc_connection` you can try resetting it:
  # gcloud services vpc-peerings update --service=servicenetworking.googleapis.com --ranges=private-ip --network=pre --project=gitlab-pre --force
  # See https://github.com/terraform-providers/terraform-provider-google/issues/3294#issuecomment-476715149
  vpc = module.network.self_link

  ip_configuration = {
    private_network = module.network.self_link
  }
}

module "preprod-db" {
  database_version = "POSTGRES_11"
  disk_autoresize  = true
  name             = "gitlab-pre"
  project          = var.project
  region           = var.region
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-sql.git?ref=v2.0.0"
  tier             = "db-custom-1-3840"
  vpc              = module.network.self_link

  backup_configuration = {
    enabled    = true
    start_time = "12:00"
  }

  ip_configuration = {
    private_network = module.network.self_link
  }

  maintenance_window_day          = 7
  maintenance_window_hour         = 0
  maintenance_window_update_track = "stable"
}

##################################
#
#  Gitaly node
#
##################################

module "gitaly" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-be-gitaly]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["gitaly"]
  machine_type          = var.machine_types["gitaly"]
  name                  = "gitaly"
  node_count            = var.node_count["gitaly"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["gitaly"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

module "thanos-compact" {
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-compact]\""
  data_disk_size            = 200
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["thanos-compact"]
  machine_type              = var.machine_types["thanos"]
  monitoring_whitelist      = var.monitoring_whitelist_thanos
  name                      = "thanos-compact"
  node_count                = var.node_count["thanos"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.2.1"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "thanos-store" {
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-store]\""
  data_disk_size            = 100
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["thanos-store"]
  machine_type              = var.machine_types["thanos"]
  monitoring_whitelist      = var.monitoring_whitelist_thanos
  name                      = "thanos-store"
  node_count                = var.node_count["thanos"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.2.1"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

############################
# Stackdriver log exclusions
############################

module "stackdriver" {
  sd_log_filters = var.sd_log_filters
  source         = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/stackdriver.git?ref=v2.0.0"
}
