#!/usr/bin/env bash
#
# Description: Fetch TF secrets from 1password
#
# Depends on 1password CLI tool: https://1password.com/downloads/command-line/

set -u -o pipefail

if ! type op >/dev/null 2>&1; then
  echo 'ERROR: Missing 1password CLI tool.'
  echo 'See: https://1password.com/downloads/command-line/'
  exit 1
fi

if ! type jq >/dev/null 2>&1; then
  echo 'ERROR: Missing jq.'
  echo 'Try apt/brew install jq'
  exit 1
fi

TF_ROOT="$(realpath "$(dirname "$(dirname "${BASH_SOURCE[0]}")")")"

uuid_jq='.[] | select(.overview.title|test("terraform-private/env_vars/.+.env")) | .uuid'
uuid_list="$(op list items --vault=Production | jq --raw-output "${uuid_jq}")"

if [[ -z "${uuid_list}" ]]; then
  echo 'ERROR: Unable to fetch items from Production vault'
  exit 1
fi

fetch_secret() {
  uuid="$1"

  echo "Fetching ${uuid}"
  item="$(op get item "${uuid}")"
  if [[ -z "${item}" ]]; then
    echo "ERROR: Unable to fetch uuid=${uuid} from 1password"
    return
  fi
  title="$(echo "${item}" | jq --raw-output '.overview.title')"
  secrets="$(echo "${item}" | jq --raw-output '.details.notesPlain')"

  secret_file="${title#terraform-}"
  if [[ -f "${secret_file}" ]]; then
    echo "Updated ${secret_file}"
  else
    echo "New ${secret_file}"
  fi
  echo -e "${secrets}" >"${TF_ROOT}/${secret_file}"
}

for uuid in ${uuid_list}; do
  fetch_secret "$uuid"
done

wait
